registerInputEvent("fxDTSBrick", "onPowerChange", "Self fxDTSBrick", 0);
registerInputEvent("fxDTSBrick", "onPowerOn", "Self fxDTSBrick", 0);
registerInputEvent("fxDTSBrick", "onPowerOff", "Self fxDTSBrick", 0);
registerOutputEvent("fxDTSBrick", "setPowerState", "bool 0");

function fxDTSBrick::onPowerChange(%this)
{
	$InputTarget_["Self"] = %this;
	%this.processInputEvent("onPowerChange");
}

function fxDTSBrick::onPowerOn(%this)
{
	$InputTarget_["Self"] = %this;
	%this.processInputEvent("onPowerOn");
}

function fxDTSBrick::onPowerOff(%this)
{
	$InputTarget_["Self"] = %this;
	%this.processInputEvent("onPowerOff");
}

function fxDTSBrick::setPowerState(%this, %state, %client)
{
	if(%this.getDatablock().isLogicEventGate)
		lualogic_sendinput(%this, 1, %state);
}

datablock fxDTSBrickData(LogicGate_EventGate_Data : brick1x1fData)
{
	category = "Logic Bricks";
	subCategory = "Special";
	uiName = "Event Gate";
	iconName = $LuaLogic::Path @ "icons/EventGate";
	brickFile = $LuaLogic::Path @ "bricks/blb/1x1f_1i_1o.blb";
	hasPrint = 1;
	printAspectRatio = "Logic";

	isLogic = true;
	isLogicGate = true;
	isLogicEventGate = true;
	
	logicUIName = "";
	logicUIDesc = "Special gate which can be used to trigger events and use events to control its output.";
	
	logicForceColor = "YELLOW";
	logicForcePrint = "ARROW";
	
	logicInput  = lualogic_readfile($LuaLogic::Path @ "bricks/special/EventGate-input.lua" );
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/special/EventGate-update.lua");

	numLogicPorts = 2;

	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 0";
	logicPortDir[0] = "0";
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "Input";
	logicPortUIDesc[0] = "Input which triggers the onPower input events";

	logicPortType[1] = 0;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = "2";
	logicPortUIName[1] = "Output";
	logicPortUIDesc[1] = "Output controlled by the setPowerState output event";
};
lualogic_registergatedefinition("LogicGate_EventGate_Data");

function LogicGate_EventGate_Data::LuaLogic_Callback(%this, %obj, %data)
{
	%obj.onPowerChange();
	if(%data)
		%obj.onPowerOn();
	else
		%obj.onPowerOff();
}

