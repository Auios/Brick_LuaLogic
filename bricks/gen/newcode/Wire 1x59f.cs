
datablock fxDtsBrickData(LogicWire_1x59f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x59f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x59f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x59f";
	
	logicBrickSize = "1 59 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
