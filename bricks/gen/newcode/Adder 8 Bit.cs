
datablock fxDtsBrickData(LogicGate_Adder8Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Adder 8 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Adder 8 Bit";
	
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Adder 8 Bit";
	logicUIName = "Adder 8 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "16 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"local val = ( " @
		"   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, 9) + Gate.getportstate(gate, 25)) " @
		" + ((Gate.getportstate(gate, 2) + Gate.getportstate(gate, 10)) * 2) " @
		" + ((Gate.getportstate(gate, 3) + Gate.getportstate(gate, 11)) * 4) " @
		" + ((Gate.getportstate(gate, 4) + Gate.getportstate(gate, 12)) * 8) " @
		" + ((Gate.getportstate(gate, 5) + Gate.getportstate(gate, 13)) * 16) " @
		" + ((Gate.getportstate(gate, 6) + Gate.getportstate(gate, 14)) * 32) " @
		" + ((Gate.getportstate(gate, 7) + Gate.getportstate(gate, 15)) * 64) " @
		" + ((Gate.getportstate(gate, 8) + Gate.getportstate(gate, 16)) * 128) " @
		") " @
		"if val >= 256 then val = val-256; Gate.setportstate(gate, 26, 1); else Gate.setportstate(gate, 26, 0) end " @
		"if val >= 128 then val = val-128; Gate.setportstate(gate, 24, 1); else Gate.setportstate(gate, 24, 0) end " @
		"if val >= 64 then val = val-64; Gate.setportstate(gate, 23, 1); else Gate.setportstate(gate, 23, 0) end " @
		"if val >= 32 then val = val-32; Gate.setportstate(gate, 22, 1); else Gate.setportstate(gate, 22, 0) end " @
		"if val >= 16 then val = val-16; Gate.setportstate(gate, 21, 1); else Gate.setportstate(gate, 21, 0) end " @
		"if val >= 8 then val = val-8; Gate.setportstate(gate, 20, 1); else Gate.setportstate(gate, 20, 0) end " @
		"if val >= 4 then val = val-4; Gate.setportstate(gate, 19, 1); else Gate.setportstate(gate, 19, 0) end " @
		"if val >= 2 then val = val-2; Gate.setportstate(gate, 18, 1); else Gate.setportstate(gate, 18, 0) end " @
		"if val >= 1 then val = val-1; Gate.setportstate(gate, 17, 1); else Gate.setportstate(gate, 17, 0) end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 26;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "15 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "13 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "11 -1 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "9 -1 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "7 -1 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "5 -1 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "3 -1 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "1 -1 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-1 -1 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "B0";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "-3 -1 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "B1";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "-5 -1 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "B2";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "-7 -1 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "B3";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "-9 -1 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "B4";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "-11 -1 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "B5";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "-13 -1 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "B6";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "-15 -1 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "B7";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 0;
	logicPortPos[16] = "15 1 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O0";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "13 1 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O1";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "11 1 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O2";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "9 1 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O3";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "7 1 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O4";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "5 1 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O5";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "3 1 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O6";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "1 1 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O7";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "15 -1 0";
	logicPortDir[24] = 2;
	logicPortUIName[24] = "CIn";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 0;
	logicPortPos[25] = "-15 -1 0";
	logicPortDir[25] = 0;
	logicPortUIName[25] = "COut";
	logicPortCauseUpdate[25] = true;
	
};
