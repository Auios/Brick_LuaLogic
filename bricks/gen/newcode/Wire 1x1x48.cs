
datablock fxDtsBrickData(LogicWire_1x1x48_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x48.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x48";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x48";
	
	logicBrickSize = "1 1 144";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
