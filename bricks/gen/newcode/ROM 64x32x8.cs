
datablock fxDtsBrickData(LogicGate_Rom64x32x8_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 64x32x8.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 64x32x8";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 64x32x8";
	logicUIName = "ROM 64x32x8";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 32 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 16383 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 20;
	
	isLogicRom = true;
	logicRomY = 32;
	logicRomZ = 8;
	logicRomX = 64;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -31 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -31 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -31 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -31 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -31 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -31 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 -31 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 -31 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 -31 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 -31 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 -31 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "A10";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "63 31 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O0";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "61 31 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O1";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "59 31 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O2";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "57 31 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O3";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "55 31 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O4";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "53 31 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O5";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "51 31 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O6";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "49 31 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O7";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "63 -31 0";
	logicPortDir[19] = 2;
	logicPortUIName[19] = "Clock";
	logicPortCauseUpdate[19] = true;
	
};
