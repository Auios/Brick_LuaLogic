
datablock fxDtsBrickData(LogicGate_Mux6Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 6 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 6 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 6 Bit Vertical";
	logicUIName = "Mux 6 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 64";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 71) then " @
		"		local idx = 7 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) + " @
		"			(Gate.getportstate(gate, 6) * 32) " @
		"		Gate.setportstate(gate, 72, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 72, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 72;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -63";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -61";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -59";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -57";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -55";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -53";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "Sel5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 0 -63";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "In0";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 0 -61";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "In1";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "0 0 -59";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "In2";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "0 0 -57";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "In3";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "0 0 -55";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "In4";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "0 0 -53";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "In5";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "0 0 -51";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "In6";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "0 0 -49";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "In7";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "0 0 -47";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "In8";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "0 0 -45";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "In9";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "0 0 -43";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "In10";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "0 0 -41";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "In11";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "0 0 -39";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "In12";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "0 0 -37";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "In13";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "0 0 -35";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "In14";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "0 0 -33";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "In15";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "0 0 -31";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "In16";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "0 0 -29";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "In17";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "0 0 -27";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "In18";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "0 0 -25";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "In19";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "0 0 -23";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "In20";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "0 0 -21";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "In21";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "0 0 -19";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "In22";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "0 0 -17";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "In23";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "0 0 -15";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "In24";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "0 0 -13";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "In25";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "0 0 -11";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "In26";
	
	logicPortType[33] = 1;
	logicPortPos[33] = "0 0 -9";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "In27";
	
	logicPortType[34] = 1;
	logicPortPos[34] = "0 0 -7";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "In28";
	
	logicPortType[35] = 1;
	logicPortPos[35] = "0 0 -5";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "In29";
	
	logicPortType[36] = 1;
	logicPortPos[36] = "0 0 -3";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "In30";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "0 0 -1";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "In31";
	
	logicPortType[38] = 1;
	logicPortPos[38] = "0 0 1";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "In32";
	
	logicPortType[39] = 1;
	logicPortPos[39] = "0 0 3";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "In33";
	
	logicPortType[40] = 1;
	logicPortPos[40] = "0 0 5";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "In34";
	
	logicPortType[41] = 1;
	logicPortPos[41] = "0 0 7";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "In35";
	
	logicPortType[42] = 1;
	logicPortPos[42] = "0 0 9";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "In36";
	
	logicPortType[43] = 1;
	logicPortPos[43] = "0 0 11";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "In37";
	
	logicPortType[44] = 1;
	logicPortPos[44] = "0 0 13";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "In38";
	
	logicPortType[45] = 1;
	logicPortPos[45] = "0 0 15";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "In39";
	
	logicPortType[46] = 1;
	logicPortPos[46] = "0 0 17";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "In40";
	
	logicPortType[47] = 1;
	logicPortPos[47] = "0 0 19";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "In41";
	
	logicPortType[48] = 1;
	logicPortPos[48] = "0 0 21";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "In42";
	
	logicPortType[49] = 1;
	logicPortPos[49] = "0 0 23";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "In43";
	
	logicPortType[50] = 1;
	logicPortPos[50] = "0 0 25";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "In44";
	
	logicPortType[51] = 1;
	logicPortPos[51] = "0 0 27";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "In45";
	
	logicPortType[52] = 1;
	logicPortPos[52] = "0 0 29";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "In46";
	
	logicPortType[53] = 1;
	logicPortPos[53] = "0 0 31";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "In47";
	
	logicPortType[54] = 1;
	logicPortPos[54] = "0 0 33";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "In48";
	
	logicPortType[55] = 1;
	logicPortPos[55] = "0 0 35";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "In49";
	
	logicPortType[56] = 1;
	logicPortPos[56] = "0 0 37";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "In50";
	
	logicPortType[57] = 1;
	logicPortPos[57] = "0 0 39";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "In51";
	
	logicPortType[58] = 1;
	logicPortPos[58] = "0 0 41";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "In52";
	
	logicPortType[59] = 1;
	logicPortPos[59] = "0 0 43";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "In53";
	
	logicPortType[60] = 1;
	logicPortPos[60] = "0 0 45";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "In54";
	
	logicPortType[61] = 1;
	logicPortPos[61] = "0 0 47";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "In55";
	
	logicPortType[62] = 1;
	logicPortPos[62] = "0 0 49";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "In56";
	
	logicPortType[63] = 1;
	logicPortPos[63] = "0 0 51";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "In57";
	
	logicPortType[64] = 1;
	logicPortPos[64] = "0 0 53";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "In58";
	
	logicPortType[65] = 1;
	logicPortPos[65] = "0 0 55";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "In59";
	
	logicPortType[66] = 1;
	logicPortPos[66] = "0 0 57";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "In60";
	
	logicPortType[67] = 1;
	logicPortPos[67] = "0 0 59";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "In61";
	
	logicPortType[68] = 1;
	logicPortPos[68] = "0 0 61";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "In62";
	
	logicPortType[69] = 1;
	logicPortPos[69] = "0 0 63";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "In63";
	
	logicPortType[70] = 1;
	logicPortPos[70] = "0 0 -63";
	logicPortDir[70] = 5;
	logicPortUIName[70] = "Enable";
	logicPortCauseUpdate[70] = true;
	
	logicPortType[71] = 0;
	logicPortPos[71] = "0 0 63";
	logicPortDir[71] = 4;
	logicPortUIName[71] = "Out";
	
};
