
datablock fxDtsBrickData(LogicWire_1x2x5f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x2x5f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x2x5f";
	
	category = "Logic Bricks";
	subCategory = "Wires Other";
	uiName = "Wire 1x2x5f";
	
	logicBrickSize = "1 2 5";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
