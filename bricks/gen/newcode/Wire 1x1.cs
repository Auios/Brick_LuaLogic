
datablock fxDtsBrickData(LogicWire_1x1_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1";
	
	logicBrickSize = "1 1 3";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
