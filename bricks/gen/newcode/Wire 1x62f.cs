
datablock fxDtsBrickData(LogicWire_1x62f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x62f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x62f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x62f";
	
	logicBrickSize = "1 62 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
