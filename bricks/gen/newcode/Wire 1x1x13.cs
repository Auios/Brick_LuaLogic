
datablock fxDtsBrickData(LogicWire_1x1x13_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x13.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x13";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x13";
	
	logicBrickSize = "1 1 39";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
