
datablock fxDtsBrickData(LogicWire_1x37f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x37f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x37f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x37f";
	
	logicBrickSize = "1 37 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
