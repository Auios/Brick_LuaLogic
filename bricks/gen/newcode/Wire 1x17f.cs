
datablock fxDtsBrickData(LogicWire_1x17f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x17f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x17f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x17f";
	
	logicBrickSize = "1 17 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
