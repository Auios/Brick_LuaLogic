
datablock fxDtsBrickData(LogicGate_GateNor2_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/NOR 2 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/NOR 2 Bit";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "NOR 2 Bit";
	logicUIName = "NOR 2 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 3, (not ( " @
		"		(Gate.getportstate(gate, 1)~=0) or " @
		"		(Gate.getportstate(gate, 2)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 3;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "-1 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 0;
	logicPortPos[2] = "1 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "Out";
	
};
