
datablock fxDtsBrickData(LogicGate_Rom64x8x64_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 64x8x64.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 64x8x64";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 64x8x64";
	logicUIName = "ROM 64x8x64";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 8 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 32767 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 74;
	
	isLogicRom = true;
	logicRomY = 8;
	logicRomZ = 64;
	logicRomX = 64;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -7 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -7 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -7 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -7 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -7 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -7 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 -7 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 -7 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 -7 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "63 7 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O0";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "61 7 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O1";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "59 7 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O2";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "57 7 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O3";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "55 7 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O4";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "53 7 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O5";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "51 7 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O6";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "49 7 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O7";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "47 7 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O8";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "45 7 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O9";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "43 7 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O10";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "41 7 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O11";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "39 7 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O12";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "37 7 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O13";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "35 7 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O14";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "33 7 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O15";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "31 7 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O16";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "29 7 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O17";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "27 7 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O18";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "25 7 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O19";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "23 7 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O20";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "21 7 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O21";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "19 7 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O22";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "17 7 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O23";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "15 7 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O24";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "13 7 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O25";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "11 7 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O26";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "9 7 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O27";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "7 7 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O28";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "5 7 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O29";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "3 7 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O30";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "1 7 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O31";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "-1 7 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "O32";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "-3 7 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "O33";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "-5 7 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "O34";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "-7 7 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "O35";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "-9 7 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "O36";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "-11 7 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "O37";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "-13 7 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "O38";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-15 7 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "O39";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-17 7 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "O40";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-19 7 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "O41";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-21 7 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "O42";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-23 7 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "O43";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-25 7 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "O44";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-27 7 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "O45";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-29 7 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "O46";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-31 7 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "O47";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-33 7 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "O48";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-35 7 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "O49";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "-37 7 0";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "O50";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "-39 7 0";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "O51";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "-41 7 0";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "O52";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "-43 7 0";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "O53";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "-45 7 0";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "O54";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "-47 7 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "O55";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "-49 7 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "O56";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "-51 7 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "O57";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "-53 7 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "O58";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "-55 7 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "O59";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "-57 7 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "O60";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "-59 7 0";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "O61";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "-61 7 0";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "O62";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "-63 7 0";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "O63";
	
	logicPortType[73] = 1;
	logicPortPos[73] = "63 -7 0";
	logicPortDir[73] = 2;
	logicPortUIName[73] = "Clock";
	logicPortCauseUpdate[73] = true;
	
};
