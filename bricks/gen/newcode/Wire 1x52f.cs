
datablock fxDtsBrickData(LogicWire_1x52f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x52f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x52f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x52f";
	
	logicBrickSize = "1 52 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
