
datablock fxDtsBrickData(LogicGate_DFlipFlop7BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 7 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 7 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 7 Bit Down";
	logicUIName = "D FlipFlop 7 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "7 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 15)~=0 then " @
		"		Gate.setportstate(gate, 8, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 9, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 10, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 11, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 12, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 13, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 14, Gate.getportstate(gate, 7)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 15;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "6 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "4 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "2 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-2 0 0";
	logicPortDir[4] = 4;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-4 0 0";
	logicPortDir[5] = 4;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-6 0 0";
	logicPortDir[6] = 4;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "6 0 0";
	logicPortDir[7] = 5;
	logicPortUIName[7] = "Out0";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "4 0 0";
	logicPortDir[8] = 5;
	logicPortUIName[8] = "Out1";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "2 0 0";
	logicPortDir[9] = 5;
	logicPortUIName[9] = "Out2";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "0 0 0";
	logicPortDir[10] = 5;
	logicPortUIName[10] = "Out3";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "-2 0 0";
	logicPortDir[11] = 5;
	logicPortUIName[11] = "Out4";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "-4 0 0";
	logicPortDir[12] = 5;
	logicPortUIName[12] = "Out5";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "-6 0 0";
	logicPortDir[13] = 5;
	logicPortUIName[13] = "Out6";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "6 0 0";
	logicPortDir[14] = 2;
	logicPortUIName[14] = "Clock";
	logicPortCauseUpdate[14] = true;
	
};
