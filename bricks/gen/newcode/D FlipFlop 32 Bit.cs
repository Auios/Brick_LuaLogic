
datablock fxDtsBrickData(LogicGate_DFlipFlop32Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 32 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 32 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 32 Bit";
	logicUIName = "D FlipFlop 32 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 65)~=0 then " @
		"		Gate.setportstate(gate, 33, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 34, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 35, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 36, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 37, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 38, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 39, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 40, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 41, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 42, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 43, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 44, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 45, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 46, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 47, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 48, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 49, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 50, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 51, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 52, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 53, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 54, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 55, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 56, Gate.getportstate(gate, 24)) " @
		"		Gate.setportstate(gate, 57, Gate.getportstate(gate, 25)) " @
		"		Gate.setportstate(gate, 58, Gate.getportstate(gate, 26)) " @
		"		Gate.setportstate(gate, 59, Gate.getportstate(gate, 27)) " @
		"		Gate.setportstate(gate, 60, Gate.getportstate(gate, 28)) " @
		"		Gate.setportstate(gate, 61, Gate.getportstate(gate, 29)) " @
		"		Gate.setportstate(gate, 62, Gate.getportstate(gate, 30)) " @
		"		Gate.setportstate(gate, 63, Gate.getportstate(gate, 31)) " @
		"		Gate.setportstate(gate, 64, Gate.getportstate(gate, 32)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 65;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "21 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "19 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "17 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "15 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "13 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "11 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "9 0 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "7 0 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "In12";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "5 0 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "In13";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "3 0 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "In14";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "1 0 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "In15";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "-1 0 0";
	logicPortDir[16] = 3;
	logicPortUIName[16] = "In16";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "-3 0 0";
	logicPortDir[17] = 3;
	logicPortUIName[17] = "In17";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "-5 0 0";
	logicPortDir[18] = 3;
	logicPortUIName[18] = "In18";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "-7 0 0";
	logicPortDir[19] = 3;
	logicPortUIName[19] = "In19";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "-9 0 0";
	logicPortDir[20] = 3;
	logicPortUIName[20] = "In20";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "-11 0 0";
	logicPortDir[21] = 3;
	logicPortUIName[21] = "In21";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "-13 0 0";
	logicPortDir[22] = 3;
	logicPortUIName[22] = "In22";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "-15 0 0";
	logicPortDir[23] = 3;
	logicPortUIName[23] = "In23";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "-17 0 0";
	logicPortDir[24] = 3;
	logicPortUIName[24] = "In24";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "-19 0 0";
	logicPortDir[25] = 3;
	logicPortUIName[25] = "In25";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "-21 0 0";
	logicPortDir[26] = 3;
	logicPortUIName[26] = "In26";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "-23 0 0";
	logicPortDir[27] = 3;
	logicPortUIName[27] = "In27";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "-25 0 0";
	logicPortDir[28] = 3;
	logicPortUIName[28] = "In28";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "-27 0 0";
	logicPortDir[29] = 3;
	logicPortUIName[29] = "In29";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "-29 0 0";
	logicPortDir[30] = 3;
	logicPortUIName[30] = "In30";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "-31 0 0";
	logicPortDir[31] = 3;
	logicPortUIName[31] = "In31";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "31 0 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "Out0";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "29 0 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "Out1";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "27 0 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "Out2";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "25 0 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "Out3";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "23 0 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "Out4";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "21 0 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "Out5";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "19 0 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "Out6";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "17 0 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "Out7";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "15 0 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "Out8";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "13 0 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "Out9";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "11 0 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "Out10";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "9 0 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "Out11";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "7 0 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "Out12";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "5 0 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "Out13";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "3 0 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "Out14";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "1 0 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "Out15";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-1 0 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "Out16";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-3 0 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "Out17";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-5 0 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "Out18";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-7 0 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "Out19";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-9 0 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "Out20";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-11 0 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "Out21";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-13 0 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "Out22";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-15 0 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "Out23";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-17 0 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "Out24";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-19 0 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "Out25";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-21 0 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "Out26";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "-23 0 0";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "Out27";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "-25 0 0";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "Out28";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "-27 0 0";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "Out29";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "-29 0 0";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "Out30";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "-31 0 0";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "Out31";
	
	logicPortType[64] = 1;
	logicPortPos[64] = "31 0 0";
	logicPortDir[64] = 2;
	logicPortUIName[64] = "Clock";
	logicPortCauseUpdate[64] = true;
	
};
