
datablock fxDtsBrickData(LogicWire_1x6f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x6f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x6f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x6f";
	
	logicBrickSize = "1 6 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
