
datablock fxDtsBrickData(LogicGate_GateNand6Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/NAND 6 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/NAND 6 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "NAND 6 Bit Vertical";
	logicUIName = "NAND 6 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 6";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 7, (not ( " @
		"		(Gate.getportstate(gate, 1)~=0) and " @
		"		(Gate.getportstate(gate, 2)~=0) and " @
		"		(Gate.getportstate(gate, 3)~=0) and " @
		"		(Gate.getportstate(gate, 4)~=0) and " @
		"		(Gate.getportstate(gate, 5)~=0) and " @
		"		(Gate.getportstate(gate, 6)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 7;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 5";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 3";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 1";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -1";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -3";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -5";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 0;
	logicPortPos[6] = "0 0 -5";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out";
	
};
