
datablock fxDtsBrickData(LogicWire_1x1x256f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x256f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x256f";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x256f";
	
	logicBrickSize = "1 1 256";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
