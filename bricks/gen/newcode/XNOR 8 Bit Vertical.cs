
datablock fxDtsBrickData(LogicGate_GateXnor8Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/XNOR 8 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/XNOR 8 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "XNOR 8 Bit Vertical";
	logicUIName = "XNOR 8 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 8";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	local v = 1 " @
		"	if Gate.getportstate(gate, 1)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 2)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 3)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 4)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 5)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 6)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 7)~=0 then v = 1 - v end " @
		"	if Gate.getportstate(gate, 8)~=0 then v = 1 - v end " @
		"	Gate.setportstate(gate, 9, v) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 9;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 7";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 5";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 3";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 1";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -1";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -3";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 0 -5";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 0 -7";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 0;
	logicPortPos[8] = "0 0 -7";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out";
	
};
