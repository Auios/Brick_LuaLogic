
datablock fxDtsBrickData(LogicGate_GateAnd7_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/AND 7 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/AND 7 Bit";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "AND 7 Bit";
	logicUIName = "AND 7 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "7 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 8, (( " @
		"		(Gate.getportstate(gate, 1)~=0) and " @
		"		(Gate.getportstate(gate, 2)~=0) and " @
		"		(Gate.getportstate(gate, 3)~=0) and " @
		"		(Gate.getportstate(gate, 4)~=0) and " @
		"		(Gate.getportstate(gate, 5)~=0) and " @
		"		(Gate.getportstate(gate, 6)~=0) and " @
		"		(Gate.getportstate(gate, 7)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 8;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "6 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "4 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "2 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-2 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-4 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-6 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 0;
	logicPortPos[7] = "6 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out";
	
};
