
datablock fxDtsBrickData(LogicWire_1x40f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x40f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x40f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x40f";
	
	logicBrickSize = "1 40 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
