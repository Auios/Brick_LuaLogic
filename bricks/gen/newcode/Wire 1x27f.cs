
datablock fxDtsBrickData(LogicWire_1x27f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x27f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x27f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x27f";
	
	logicBrickSize = "1 27 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
