
datablock fxDtsBrickData(LogicGate_Mux8Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 8 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 8 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 8 Bit Vertical";
	logicUIName = "Mux 8 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 256";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 265) then " @
		"		local idx = 9 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) + " @
		"			(Gate.getportstate(gate, 6) * 32) + " @
		"			(Gate.getportstate(gate, 7) * 64) + " @
		"			(Gate.getportstate(gate, 8) * 128) " @
		"		Gate.setportstate(gate, 266, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 266, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 266;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -255";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -253";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -251";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -249";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -247";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -245";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "Sel5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 0 -243";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "Sel6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 0 -241";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "Sel7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "0 0 -255";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "In0";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "0 0 -253";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "In1";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "0 0 -251";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "In2";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "0 0 -249";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "In3";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "0 0 -247";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "In4";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "0 0 -245";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "In5";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "0 0 -243";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "In6";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "0 0 -241";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "In7";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "0 0 -239";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "In8";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "0 0 -237";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "In9";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "0 0 -235";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "In10";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "0 0 -233";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "In11";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "0 0 -231";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "In12";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "0 0 -229";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "In13";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "0 0 -227";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "In14";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "0 0 -225";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "In15";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "0 0 -223";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "In16";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "0 0 -221";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "In17";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "0 0 -219";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "In18";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "0 0 -217";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "In19";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "0 0 -215";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "In20";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "0 0 -213";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "In21";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "0 0 -211";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "In22";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "0 0 -209";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "In23";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "0 0 -207";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "In24";
	
	logicPortType[33] = 1;
	logicPortPos[33] = "0 0 -205";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "In25";
	
	logicPortType[34] = 1;
	logicPortPos[34] = "0 0 -203";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "In26";
	
	logicPortType[35] = 1;
	logicPortPos[35] = "0 0 -201";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "In27";
	
	logicPortType[36] = 1;
	logicPortPos[36] = "0 0 -199";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "In28";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "0 0 -197";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "In29";
	
	logicPortType[38] = 1;
	logicPortPos[38] = "0 0 -195";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "In30";
	
	logicPortType[39] = 1;
	logicPortPos[39] = "0 0 -193";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "In31";
	
	logicPortType[40] = 1;
	logicPortPos[40] = "0 0 -191";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "In32";
	
	logicPortType[41] = 1;
	logicPortPos[41] = "0 0 -189";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "In33";
	
	logicPortType[42] = 1;
	logicPortPos[42] = "0 0 -187";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "In34";
	
	logicPortType[43] = 1;
	logicPortPos[43] = "0 0 -185";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "In35";
	
	logicPortType[44] = 1;
	logicPortPos[44] = "0 0 -183";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "In36";
	
	logicPortType[45] = 1;
	logicPortPos[45] = "0 0 -181";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "In37";
	
	logicPortType[46] = 1;
	logicPortPos[46] = "0 0 -179";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "In38";
	
	logicPortType[47] = 1;
	logicPortPos[47] = "0 0 -177";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "In39";
	
	logicPortType[48] = 1;
	logicPortPos[48] = "0 0 -175";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "In40";
	
	logicPortType[49] = 1;
	logicPortPos[49] = "0 0 -173";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "In41";
	
	logicPortType[50] = 1;
	logicPortPos[50] = "0 0 -171";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "In42";
	
	logicPortType[51] = 1;
	logicPortPos[51] = "0 0 -169";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "In43";
	
	logicPortType[52] = 1;
	logicPortPos[52] = "0 0 -167";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "In44";
	
	logicPortType[53] = 1;
	logicPortPos[53] = "0 0 -165";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "In45";
	
	logicPortType[54] = 1;
	logicPortPos[54] = "0 0 -163";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "In46";
	
	logicPortType[55] = 1;
	logicPortPos[55] = "0 0 -161";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "In47";
	
	logicPortType[56] = 1;
	logicPortPos[56] = "0 0 -159";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "In48";
	
	logicPortType[57] = 1;
	logicPortPos[57] = "0 0 -157";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "In49";
	
	logicPortType[58] = 1;
	logicPortPos[58] = "0 0 -155";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "In50";
	
	logicPortType[59] = 1;
	logicPortPos[59] = "0 0 -153";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "In51";
	
	logicPortType[60] = 1;
	logicPortPos[60] = "0 0 -151";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "In52";
	
	logicPortType[61] = 1;
	logicPortPos[61] = "0 0 -149";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "In53";
	
	logicPortType[62] = 1;
	logicPortPos[62] = "0 0 -147";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "In54";
	
	logicPortType[63] = 1;
	logicPortPos[63] = "0 0 -145";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "In55";
	
	logicPortType[64] = 1;
	logicPortPos[64] = "0 0 -143";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "In56";
	
	logicPortType[65] = 1;
	logicPortPos[65] = "0 0 -141";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "In57";
	
	logicPortType[66] = 1;
	logicPortPos[66] = "0 0 -139";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "In58";
	
	logicPortType[67] = 1;
	logicPortPos[67] = "0 0 -137";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "In59";
	
	logicPortType[68] = 1;
	logicPortPos[68] = "0 0 -135";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "In60";
	
	logicPortType[69] = 1;
	logicPortPos[69] = "0 0 -133";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "In61";
	
	logicPortType[70] = 1;
	logicPortPos[70] = "0 0 -131";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "In62";
	
	logicPortType[71] = 1;
	logicPortPos[71] = "0 0 -129";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "In63";
	
	logicPortType[72] = 1;
	logicPortPos[72] = "0 0 -127";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "In64";
	
	logicPortType[73] = 1;
	logicPortPos[73] = "0 0 -125";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "In65";
	
	logicPortType[74] = 1;
	logicPortPos[74] = "0 0 -123";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "In66";
	
	logicPortType[75] = 1;
	logicPortPos[75] = "0 0 -121";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "In67";
	
	logicPortType[76] = 1;
	logicPortPos[76] = "0 0 -119";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "In68";
	
	logicPortType[77] = 1;
	logicPortPos[77] = "0 0 -117";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "In69";
	
	logicPortType[78] = 1;
	logicPortPos[78] = "0 0 -115";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "In70";
	
	logicPortType[79] = 1;
	logicPortPos[79] = "0 0 -113";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "In71";
	
	logicPortType[80] = 1;
	logicPortPos[80] = "0 0 -111";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "In72";
	
	logicPortType[81] = 1;
	logicPortPos[81] = "0 0 -109";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "In73";
	
	logicPortType[82] = 1;
	logicPortPos[82] = "0 0 -107";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "In74";
	
	logicPortType[83] = 1;
	logicPortPos[83] = "0 0 -105";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "In75";
	
	logicPortType[84] = 1;
	logicPortPos[84] = "0 0 -103";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "In76";
	
	logicPortType[85] = 1;
	logicPortPos[85] = "0 0 -101";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "In77";
	
	logicPortType[86] = 1;
	logicPortPos[86] = "0 0 -99";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "In78";
	
	logicPortType[87] = 1;
	logicPortPos[87] = "0 0 -97";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "In79";
	
	logicPortType[88] = 1;
	logicPortPos[88] = "0 0 -95";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "In80";
	
	logicPortType[89] = 1;
	logicPortPos[89] = "0 0 -93";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "In81";
	
	logicPortType[90] = 1;
	logicPortPos[90] = "0 0 -91";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "In82";
	
	logicPortType[91] = 1;
	logicPortPos[91] = "0 0 -89";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "In83";
	
	logicPortType[92] = 1;
	logicPortPos[92] = "0 0 -87";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "In84";
	
	logicPortType[93] = 1;
	logicPortPos[93] = "0 0 -85";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "In85";
	
	logicPortType[94] = 1;
	logicPortPos[94] = "0 0 -83";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "In86";
	
	logicPortType[95] = 1;
	logicPortPos[95] = "0 0 -81";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "In87";
	
	logicPortType[96] = 1;
	logicPortPos[96] = "0 0 -79";
	logicPortDir[96] = 1;
	logicPortUIName[96] = "In88";
	
	logicPortType[97] = 1;
	logicPortPos[97] = "0 0 -77";
	logicPortDir[97] = 1;
	logicPortUIName[97] = "In89";
	
	logicPortType[98] = 1;
	logicPortPos[98] = "0 0 -75";
	logicPortDir[98] = 1;
	logicPortUIName[98] = "In90";
	
	logicPortType[99] = 1;
	logicPortPos[99] = "0 0 -73";
	logicPortDir[99] = 1;
	logicPortUIName[99] = "In91";
	
	logicPortType[100] = 1;
	logicPortPos[100] = "0 0 -71";
	logicPortDir[100] = 1;
	logicPortUIName[100] = "In92";
	
	logicPortType[101] = 1;
	logicPortPos[101] = "0 0 -69";
	logicPortDir[101] = 1;
	logicPortUIName[101] = "In93";
	
	logicPortType[102] = 1;
	logicPortPos[102] = "0 0 -67";
	logicPortDir[102] = 1;
	logicPortUIName[102] = "In94";
	
	logicPortType[103] = 1;
	logicPortPos[103] = "0 0 -65";
	logicPortDir[103] = 1;
	logicPortUIName[103] = "In95";
	
	logicPortType[104] = 1;
	logicPortPos[104] = "0 0 -63";
	logicPortDir[104] = 1;
	logicPortUIName[104] = "In96";
	
	logicPortType[105] = 1;
	logicPortPos[105] = "0 0 -61";
	logicPortDir[105] = 1;
	logicPortUIName[105] = "In97";
	
	logicPortType[106] = 1;
	logicPortPos[106] = "0 0 -59";
	logicPortDir[106] = 1;
	logicPortUIName[106] = "In98";
	
	logicPortType[107] = 1;
	logicPortPos[107] = "0 0 -57";
	logicPortDir[107] = 1;
	logicPortUIName[107] = "In99";
	
	logicPortType[108] = 1;
	logicPortPos[108] = "0 0 -55";
	logicPortDir[108] = 1;
	logicPortUIName[108] = "In100";
	
	logicPortType[109] = 1;
	logicPortPos[109] = "0 0 -53";
	logicPortDir[109] = 1;
	logicPortUIName[109] = "In101";
	
	logicPortType[110] = 1;
	logicPortPos[110] = "0 0 -51";
	logicPortDir[110] = 1;
	logicPortUIName[110] = "In102";
	
	logicPortType[111] = 1;
	logicPortPos[111] = "0 0 -49";
	logicPortDir[111] = 1;
	logicPortUIName[111] = "In103";
	
	logicPortType[112] = 1;
	logicPortPos[112] = "0 0 -47";
	logicPortDir[112] = 1;
	logicPortUIName[112] = "In104";
	
	logicPortType[113] = 1;
	logicPortPos[113] = "0 0 -45";
	logicPortDir[113] = 1;
	logicPortUIName[113] = "In105";
	
	logicPortType[114] = 1;
	logicPortPos[114] = "0 0 -43";
	logicPortDir[114] = 1;
	logicPortUIName[114] = "In106";
	
	logicPortType[115] = 1;
	logicPortPos[115] = "0 0 -41";
	logicPortDir[115] = 1;
	logicPortUIName[115] = "In107";
	
	logicPortType[116] = 1;
	logicPortPos[116] = "0 0 -39";
	logicPortDir[116] = 1;
	logicPortUIName[116] = "In108";
	
	logicPortType[117] = 1;
	logicPortPos[117] = "0 0 -37";
	logicPortDir[117] = 1;
	logicPortUIName[117] = "In109";
	
	logicPortType[118] = 1;
	logicPortPos[118] = "0 0 -35";
	logicPortDir[118] = 1;
	logicPortUIName[118] = "In110";
	
	logicPortType[119] = 1;
	logicPortPos[119] = "0 0 -33";
	logicPortDir[119] = 1;
	logicPortUIName[119] = "In111";
	
	logicPortType[120] = 1;
	logicPortPos[120] = "0 0 -31";
	logicPortDir[120] = 1;
	logicPortUIName[120] = "In112";
	
	logicPortType[121] = 1;
	logicPortPos[121] = "0 0 -29";
	logicPortDir[121] = 1;
	logicPortUIName[121] = "In113";
	
	logicPortType[122] = 1;
	logicPortPos[122] = "0 0 -27";
	logicPortDir[122] = 1;
	logicPortUIName[122] = "In114";
	
	logicPortType[123] = 1;
	logicPortPos[123] = "0 0 -25";
	logicPortDir[123] = 1;
	logicPortUIName[123] = "In115";
	
	logicPortType[124] = 1;
	logicPortPos[124] = "0 0 -23";
	logicPortDir[124] = 1;
	logicPortUIName[124] = "In116";
	
	logicPortType[125] = 1;
	logicPortPos[125] = "0 0 -21";
	logicPortDir[125] = 1;
	logicPortUIName[125] = "In117";
	
	logicPortType[126] = 1;
	logicPortPos[126] = "0 0 -19";
	logicPortDir[126] = 1;
	logicPortUIName[126] = "In118";
	
	logicPortType[127] = 1;
	logicPortPos[127] = "0 0 -17";
	logicPortDir[127] = 1;
	logicPortUIName[127] = "In119";
	
	logicPortType[128] = 1;
	logicPortPos[128] = "0 0 -15";
	logicPortDir[128] = 1;
	logicPortUIName[128] = "In120";
	
	logicPortType[129] = 1;
	logicPortPos[129] = "0 0 -13";
	logicPortDir[129] = 1;
	logicPortUIName[129] = "In121";
	
	logicPortType[130] = 1;
	logicPortPos[130] = "0 0 -11";
	logicPortDir[130] = 1;
	logicPortUIName[130] = "In122";
	
	logicPortType[131] = 1;
	logicPortPos[131] = "0 0 -9";
	logicPortDir[131] = 1;
	logicPortUIName[131] = "In123";
	
	logicPortType[132] = 1;
	logicPortPos[132] = "0 0 -7";
	logicPortDir[132] = 1;
	logicPortUIName[132] = "In124";
	
	logicPortType[133] = 1;
	logicPortPos[133] = "0 0 -5";
	logicPortDir[133] = 1;
	logicPortUIName[133] = "In125";
	
	logicPortType[134] = 1;
	logicPortPos[134] = "0 0 -3";
	logicPortDir[134] = 1;
	logicPortUIName[134] = "In126";
	
	logicPortType[135] = 1;
	logicPortPos[135] = "0 0 -1";
	logicPortDir[135] = 1;
	logicPortUIName[135] = "In127";
	
	logicPortType[136] = 1;
	logicPortPos[136] = "0 0 1";
	logicPortDir[136] = 1;
	logicPortUIName[136] = "In128";
	
	logicPortType[137] = 1;
	logicPortPos[137] = "0 0 3";
	logicPortDir[137] = 1;
	logicPortUIName[137] = "In129";
	
	logicPortType[138] = 1;
	logicPortPos[138] = "0 0 5";
	logicPortDir[138] = 1;
	logicPortUIName[138] = "In130";
	
	logicPortType[139] = 1;
	logicPortPos[139] = "0 0 7";
	logicPortDir[139] = 1;
	logicPortUIName[139] = "In131";
	
	logicPortType[140] = 1;
	logicPortPos[140] = "0 0 9";
	logicPortDir[140] = 1;
	logicPortUIName[140] = "In132";
	
	logicPortType[141] = 1;
	logicPortPos[141] = "0 0 11";
	logicPortDir[141] = 1;
	logicPortUIName[141] = "In133";
	
	logicPortType[142] = 1;
	logicPortPos[142] = "0 0 13";
	logicPortDir[142] = 1;
	logicPortUIName[142] = "In134";
	
	logicPortType[143] = 1;
	logicPortPos[143] = "0 0 15";
	logicPortDir[143] = 1;
	logicPortUIName[143] = "In135";
	
	logicPortType[144] = 1;
	logicPortPos[144] = "0 0 17";
	logicPortDir[144] = 1;
	logicPortUIName[144] = "In136";
	
	logicPortType[145] = 1;
	logicPortPos[145] = "0 0 19";
	logicPortDir[145] = 1;
	logicPortUIName[145] = "In137";
	
	logicPortType[146] = 1;
	logicPortPos[146] = "0 0 21";
	logicPortDir[146] = 1;
	logicPortUIName[146] = "In138";
	
	logicPortType[147] = 1;
	logicPortPos[147] = "0 0 23";
	logicPortDir[147] = 1;
	logicPortUIName[147] = "In139";
	
	logicPortType[148] = 1;
	logicPortPos[148] = "0 0 25";
	logicPortDir[148] = 1;
	logicPortUIName[148] = "In140";
	
	logicPortType[149] = 1;
	logicPortPos[149] = "0 0 27";
	logicPortDir[149] = 1;
	logicPortUIName[149] = "In141";
	
	logicPortType[150] = 1;
	logicPortPos[150] = "0 0 29";
	logicPortDir[150] = 1;
	logicPortUIName[150] = "In142";
	
	logicPortType[151] = 1;
	logicPortPos[151] = "0 0 31";
	logicPortDir[151] = 1;
	logicPortUIName[151] = "In143";
	
	logicPortType[152] = 1;
	logicPortPos[152] = "0 0 33";
	logicPortDir[152] = 1;
	logicPortUIName[152] = "In144";
	
	logicPortType[153] = 1;
	logicPortPos[153] = "0 0 35";
	logicPortDir[153] = 1;
	logicPortUIName[153] = "In145";
	
	logicPortType[154] = 1;
	logicPortPos[154] = "0 0 37";
	logicPortDir[154] = 1;
	logicPortUIName[154] = "In146";
	
	logicPortType[155] = 1;
	logicPortPos[155] = "0 0 39";
	logicPortDir[155] = 1;
	logicPortUIName[155] = "In147";
	
	logicPortType[156] = 1;
	logicPortPos[156] = "0 0 41";
	logicPortDir[156] = 1;
	logicPortUIName[156] = "In148";
	
	logicPortType[157] = 1;
	logicPortPos[157] = "0 0 43";
	logicPortDir[157] = 1;
	logicPortUIName[157] = "In149";
	
	logicPortType[158] = 1;
	logicPortPos[158] = "0 0 45";
	logicPortDir[158] = 1;
	logicPortUIName[158] = "In150";
	
	logicPortType[159] = 1;
	logicPortPos[159] = "0 0 47";
	logicPortDir[159] = 1;
	logicPortUIName[159] = "In151";
	
	logicPortType[160] = 1;
	logicPortPos[160] = "0 0 49";
	logicPortDir[160] = 1;
	logicPortUIName[160] = "In152";
	
	logicPortType[161] = 1;
	logicPortPos[161] = "0 0 51";
	logicPortDir[161] = 1;
	logicPortUIName[161] = "In153";
	
	logicPortType[162] = 1;
	logicPortPos[162] = "0 0 53";
	logicPortDir[162] = 1;
	logicPortUIName[162] = "In154";
	
	logicPortType[163] = 1;
	logicPortPos[163] = "0 0 55";
	logicPortDir[163] = 1;
	logicPortUIName[163] = "In155";
	
	logicPortType[164] = 1;
	logicPortPos[164] = "0 0 57";
	logicPortDir[164] = 1;
	logicPortUIName[164] = "In156";
	
	logicPortType[165] = 1;
	logicPortPos[165] = "0 0 59";
	logicPortDir[165] = 1;
	logicPortUIName[165] = "In157";
	
	logicPortType[166] = 1;
	logicPortPos[166] = "0 0 61";
	logicPortDir[166] = 1;
	logicPortUIName[166] = "In158";
	
	logicPortType[167] = 1;
	logicPortPos[167] = "0 0 63";
	logicPortDir[167] = 1;
	logicPortUIName[167] = "In159";
	
	logicPortType[168] = 1;
	logicPortPos[168] = "0 0 65";
	logicPortDir[168] = 1;
	logicPortUIName[168] = "In160";
	
	logicPortType[169] = 1;
	logicPortPos[169] = "0 0 67";
	logicPortDir[169] = 1;
	logicPortUIName[169] = "In161";
	
	logicPortType[170] = 1;
	logicPortPos[170] = "0 0 69";
	logicPortDir[170] = 1;
	logicPortUIName[170] = "In162";
	
	logicPortType[171] = 1;
	logicPortPos[171] = "0 0 71";
	logicPortDir[171] = 1;
	logicPortUIName[171] = "In163";
	
	logicPortType[172] = 1;
	logicPortPos[172] = "0 0 73";
	logicPortDir[172] = 1;
	logicPortUIName[172] = "In164";
	
	logicPortType[173] = 1;
	logicPortPos[173] = "0 0 75";
	logicPortDir[173] = 1;
	logicPortUIName[173] = "In165";
	
	logicPortType[174] = 1;
	logicPortPos[174] = "0 0 77";
	logicPortDir[174] = 1;
	logicPortUIName[174] = "In166";
	
	logicPortType[175] = 1;
	logicPortPos[175] = "0 0 79";
	logicPortDir[175] = 1;
	logicPortUIName[175] = "In167";
	
	logicPortType[176] = 1;
	logicPortPos[176] = "0 0 81";
	logicPortDir[176] = 1;
	logicPortUIName[176] = "In168";
	
	logicPortType[177] = 1;
	logicPortPos[177] = "0 0 83";
	logicPortDir[177] = 1;
	logicPortUIName[177] = "In169";
	
	logicPortType[178] = 1;
	logicPortPos[178] = "0 0 85";
	logicPortDir[178] = 1;
	logicPortUIName[178] = "In170";
	
	logicPortType[179] = 1;
	logicPortPos[179] = "0 0 87";
	logicPortDir[179] = 1;
	logicPortUIName[179] = "In171";
	
	logicPortType[180] = 1;
	logicPortPos[180] = "0 0 89";
	logicPortDir[180] = 1;
	logicPortUIName[180] = "In172";
	
	logicPortType[181] = 1;
	logicPortPos[181] = "0 0 91";
	logicPortDir[181] = 1;
	logicPortUIName[181] = "In173";
	
	logicPortType[182] = 1;
	logicPortPos[182] = "0 0 93";
	logicPortDir[182] = 1;
	logicPortUIName[182] = "In174";
	
	logicPortType[183] = 1;
	logicPortPos[183] = "0 0 95";
	logicPortDir[183] = 1;
	logicPortUIName[183] = "In175";
	
	logicPortType[184] = 1;
	logicPortPos[184] = "0 0 97";
	logicPortDir[184] = 1;
	logicPortUIName[184] = "In176";
	
	logicPortType[185] = 1;
	logicPortPos[185] = "0 0 99";
	logicPortDir[185] = 1;
	logicPortUIName[185] = "In177";
	
	logicPortType[186] = 1;
	logicPortPos[186] = "0 0 101";
	logicPortDir[186] = 1;
	logicPortUIName[186] = "In178";
	
	logicPortType[187] = 1;
	logicPortPos[187] = "0 0 103";
	logicPortDir[187] = 1;
	logicPortUIName[187] = "In179";
	
	logicPortType[188] = 1;
	logicPortPos[188] = "0 0 105";
	logicPortDir[188] = 1;
	logicPortUIName[188] = "In180";
	
	logicPortType[189] = 1;
	logicPortPos[189] = "0 0 107";
	logicPortDir[189] = 1;
	logicPortUIName[189] = "In181";
	
	logicPortType[190] = 1;
	logicPortPos[190] = "0 0 109";
	logicPortDir[190] = 1;
	logicPortUIName[190] = "In182";
	
	logicPortType[191] = 1;
	logicPortPos[191] = "0 0 111";
	logicPortDir[191] = 1;
	logicPortUIName[191] = "In183";
	
	logicPortType[192] = 1;
	logicPortPos[192] = "0 0 113";
	logicPortDir[192] = 1;
	logicPortUIName[192] = "In184";
	
	logicPortType[193] = 1;
	logicPortPos[193] = "0 0 115";
	logicPortDir[193] = 1;
	logicPortUIName[193] = "In185";
	
	logicPortType[194] = 1;
	logicPortPos[194] = "0 0 117";
	logicPortDir[194] = 1;
	logicPortUIName[194] = "In186";
	
	logicPortType[195] = 1;
	logicPortPos[195] = "0 0 119";
	logicPortDir[195] = 1;
	logicPortUIName[195] = "In187";
	
	logicPortType[196] = 1;
	logicPortPos[196] = "0 0 121";
	logicPortDir[196] = 1;
	logicPortUIName[196] = "In188";
	
	logicPortType[197] = 1;
	logicPortPos[197] = "0 0 123";
	logicPortDir[197] = 1;
	logicPortUIName[197] = "In189";
	
	logicPortType[198] = 1;
	logicPortPos[198] = "0 0 125";
	logicPortDir[198] = 1;
	logicPortUIName[198] = "In190";
	
	logicPortType[199] = 1;
	logicPortPos[199] = "0 0 127";
	logicPortDir[199] = 1;
	logicPortUIName[199] = "In191";
	
	logicPortType[200] = 1;
	logicPortPos[200] = "0 0 129";
	logicPortDir[200] = 1;
	logicPortUIName[200] = "In192";
	
	logicPortType[201] = 1;
	logicPortPos[201] = "0 0 131";
	logicPortDir[201] = 1;
	logicPortUIName[201] = "In193";
	
	logicPortType[202] = 1;
	logicPortPos[202] = "0 0 133";
	logicPortDir[202] = 1;
	logicPortUIName[202] = "In194";
	
	logicPortType[203] = 1;
	logicPortPos[203] = "0 0 135";
	logicPortDir[203] = 1;
	logicPortUIName[203] = "In195";
	
	logicPortType[204] = 1;
	logicPortPos[204] = "0 0 137";
	logicPortDir[204] = 1;
	logicPortUIName[204] = "In196";
	
	logicPortType[205] = 1;
	logicPortPos[205] = "0 0 139";
	logicPortDir[205] = 1;
	logicPortUIName[205] = "In197";
	
	logicPortType[206] = 1;
	logicPortPos[206] = "0 0 141";
	logicPortDir[206] = 1;
	logicPortUIName[206] = "In198";
	
	logicPortType[207] = 1;
	logicPortPos[207] = "0 0 143";
	logicPortDir[207] = 1;
	logicPortUIName[207] = "In199";
	
	logicPortType[208] = 1;
	logicPortPos[208] = "0 0 145";
	logicPortDir[208] = 1;
	logicPortUIName[208] = "In200";
	
	logicPortType[209] = 1;
	logicPortPos[209] = "0 0 147";
	logicPortDir[209] = 1;
	logicPortUIName[209] = "In201";
	
	logicPortType[210] = 1;
	logicPortPos[210] = "0 0 149";
	logicPortDir[210] = 1;
	logicPortUIName[210] = "In202";
	
	logicPortType[211] = 1;
	logicPortPos[211] = "0 0 151";
	logicPortDir[211] = 1;
	logicPortUIName[211] = "In203";
	
	logicPortType[212] = 1;
	logicPortPos[212] = "0 0 153";
	logicPortDir[212] = 1;
	logicPortUIName[212] = "In204";
	
	logicPortType[213] = 1;
	logicPortPos[213] = "0 0 155";
	logicPortDir[213] = 1;
	logicPortUIName[213] = "In205";
	
	logicPortType[214] = 1;
	logicPortPos[214] = "0 0 157";
	logicPortDir[214] = 1;
	logicPortUIName[214] = "In206";
	
	logicPortType[215] = 1;
	logicPortPos[215] = "0 0 159";
	logicPortDir[215] = 1;
	logicPortUIName[215] = "In207";
	
	logicPortType[216] = 1;
	logicPortPos[216] = "0 0 161";
	logicPortDir[216] = 1;
	logicPortUIName[216] = "In208";
	
	logicPortType[217] = 1;
	logicPortPos[217] = "0 0 163";
	logicPortDir[217] = 1;
	logicPortUIName[217] = "In209";
	
	logicPortType[218] = 1;
	logicPortPos[218] = "0 0 165";
	logicPortDir[218] = 1;
	logicPortUIName[218] = "In210";
	
	logicPortType[219] = 1;
	logicPortPos[219] = "0 0 167";
	logicPortDir[219] = 1;
	logicPortUIName[219] = "In211";
	
	logicPortType[220] = 1;
	logicPortPos[220] = "0 0 169";
	logicPortDir[220] = 1;
	logicPortUIName[220] = "In212";
	
	logicPortType[221] = 1;
	logicPortPos[221] = "0 0 171";
	logicPortDir[221] = 1;
	logicPortUIName[221] = "In213";
	
	logicPortType[222] = 1;
	logicPortPos[222] = "0 0 173";
	logicPortDir[222] = 1;
	logicPortUIName[222] = "In214";
	
	logicPortType[223] = 1;
	logicPortPos[223] = "0 0 175";
	logicPortDir[223] = 1;
	logicPortUIName[223] = "In215";
	
	logicPortType[224] = 1;
	logicPortPos[224] = "0 0 177";
	logicPortDir[224] = 1;
	logicPortUIName[224] = "In216";
	
	logicPortType[225] = 1;
	logicPortPos[225] = "0 0 179";
	logicPortDir[225] = 1;
	logicPortUIName[225] = "In217";
	
	logicPortType[226] = 1;
	logicPortPos[226] = "0 0 181";
	logicPortDir[226] = 1;
	logicPortUIName[226] = "In218";
	
	logicPortType[227] = 1;
	logicPortPos[227] = "0 0 183";
	logicPortDir[227] = 1;
	logicPortUIName[227] = "In219";
	
	logicPortType[228] = 1;
	logicPortPos[228] = "0 0 185";
	logicPortDir[228] = 1;
	logicPortUIName[228] = "In220";
	
	logicPortType[229] = 1;
	logicPortPos[229] = "0 0 187";
	logicPortDir[229] = 1;
	logicPortUIName[229] = "In221";
	
	logicPortType[230] = 1;
	logicPortPos[230] = "0 0 189";
	logicPortDir[230] = 1;
	logicPortUIName[230] = "In222";
	
	logicPortType[231] = 1;
	logicPortPos[231] = "0 0 191";
	logicPortDir[231] = 1;
	logicPortUIName[231] = "In223";
	
	logicPortType[232] = 1;
	logicPortPos[232] = "0 0 193";
	logicPortDir[232] = 1;
	logicPortUIName[232] = "In224";
	
	logicPortType[233] = 1;
	logicPortPos[233] = "0 0 195";
	logicPortDir[233] = 1;
	logicPortUIName[233] = "In225";
	
	logicPortType[234] = 1;
	logicPortPos[234] = "0 0 197";
	logicPortDir[234] = 1;
	logicPortUIName[234] = "In226";
	
	logicPortType[235] = 1;
	logicPortPos[235] = "0 0 199";
	logicPortDir[235] = 1;
	logicPortUIName[235] = "In227";
	
	logicPortType[236] = 1;
	logicPortPos[236] = "0 0 201";
	logicPortDir[236] = 1;
	logicPortUIName[236] = "In228";
	
	logicPortType[237] = 1;
	logicPortPos[237] = "0 0 203";
	logicPortDir[237] = 1;
	logicPortUIName[237] = "In229";
	
	logicPortType[238] = 1;
	logicPortPos[238] = "0 0 205";
	logicPortDir[238] = 1;
	logicPortUIName[238] = "In230";
	
	logicPortType[239] = 1;
	logicPortPos[239] = "0 0 207";
	logicPortDir[239] = 1;
	logicPortUIName[239] = "In231";
	
	logicPortType[240] = 1;
	logicPortPos[240] = "0 0 209";
	logicPortDir[240] = 1;
	logicPortUIName[240] = "In232";
	
	logicPortType[241] = 1;
	logicPortPos[241] = "0 0 211";
	logicPortDir[241] = 1;
	logicPortUIName[241] = "In233";
	
	logicPortType[242] = 1;
	logicPortPos[242] = "0 0 213";
	logicPortDir[242] = 1;
	logicPortUIName[242] = "In234";
	
	logicPortType[243] = 1;
	logicPortPos[243] = "0 0 215";
	logicPortDir[243] = 1;
	logicPortUIName[243] = "In235";
	
	logicPortType[244] = 1;
	logicPortPos[244] = "0 0 217";
	logicPortDir[244] = 1;
	logicPortUIName[244] = "In236";
	
	logicPortType[245] = 1;
	logicPortPos[245] = "0 0 219";
	logicPortDir[245] = 1;
	logicPortUIName[245] = "In237";
	
	logicPortType[246] = 1;
	logicPortPos[246] = "0 0 221";
	logicPortDir[246] = 1;
	logicPortUIName[246] = "In238";
	
	logicPortType[247] = 1;
	logicPortPos[247] = "0 0 223";
	logicPortDir[247] = 1;
	logicPortUIName[247] = "In239";
	
	logicPortType[248] = 1;
	logicPortPos[248] = "0 0 225";
	logicPortDir[248] = 1;
	logicPortUIName[248] = "In240";
	
	logicPortType[249] = 1;
	logicPortPos[249] = "0 0 227";
	logicPortDir[249] = 1;
	logicPortUIName[249] = "In241";
	
	logicPortType[250] = 1;
	logicPortPos[250] = "0 0 229";
	logicPortDir[250] = 1;
	logicPortUIName[250] = "In242";
	
	logicPortType[251] = 1;
	logicPortPos[251] = "0 0 231";
	logicPortDir[251] = 1;
	logicPortUIName[251] = "In243";
	
	logicPortType[252] = 1;
	logicPortPos[252] = "0 0 233";
	logicPortDir[252] = 1;
	logicPortUIName[252] = "In244";
	
	logicPortType[253] = 1;
	logicPortPos[253] = "0 0 235";
	logicPortDir[253] = 1;
	logicPortUIName[253] = "In245";
	
	logicPortType[254] = 1;
	logicPortPos[254] = "0 0 237";
	logicPortDir[254] = 1;
	logicPortUIName[254] = "In246";
	
	logicPortType[255] = 1;
	logicPortPos[255] = "0 0 239";
	logicPortDir[255] = 1;
	logicPortUIName[255] = "In247";
	
	logicPortType[256] = 1;
	logicPortPos[256] = "0 0 241";
	logicPortDir[256] = 1;
	logicPortUIName[256] = "In248";
	
	logicPortType[257] = 1;
	logicPortPos[257] = "0 0 243";
	logicPortDir[257] = 1;
	logicPortUIName[257] = "In249";
	
	logicPortType[258] = 1;
	logicPortPos[258] = "0 0 245";
	logicPortDir[258] = 1;
	logicPortUIName[258] = "In250";
	
	logicPortType[259] = 1;
	logicPortPos[259] = "0 0 247";
	logicPortDir[259] = 1;
	logicPortUIName[259] = "In251";
	
	logicPortType[260] = 1;
	logicPortPos[260] = "0 0 249";
	logicPortDir[260] = 1;
	logicPortUIName[260] = "In252";
	
	logicPortType[261] = 1;
	logicPortPos[261] = "0 0 251";
	logicPortDir[261] = 1;
	logicPortUIName[261] = "In253";
	
	logicPortType[262] = 1;
	logicPortPos[262] = "0 0 253";
	logicPortDir[262] = 1;
	logicPortUIName[262] = "In254";
	
	logicPortType[263] = 1;
	logicPortPos[263] = "0 0 255";
	logicPortDir[263] = 1;
	logicPortUIName[263] = "In255";
	
	logicPortType[264] = 1;
	logicPortPos[264] = "0 0 -255";
	logicPortDir[264] = 5;
	logicPortUIName[264] = "Enable";
	logicPortCauseUpdate[264] = true;
	
	logicPortType[265] = 0;
	logicPortPos[265] = "0 0 255";
	logicPortDir[265] = 4;
	logicPortUIName[265] = "Out";
	
};
