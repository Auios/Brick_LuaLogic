
datablock fxDtsBrickData(LogicWire_1x25f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x25f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x25f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x25f";
	
	logicBrickSize = "1 25 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
