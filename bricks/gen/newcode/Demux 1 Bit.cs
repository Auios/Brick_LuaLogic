
datablock fxDtsBrickData(LogicGate_Demux1_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 1 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 1 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 1 Bit";
	logicUIName = "Demux 1 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 2 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 4)~=0 then " @
		"		local idx = 2 + " @
		"			(Gate.getportstate(gate, 1) * 1) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 4;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 0;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 1;
	logicPortUIName[1] = "Out0";
	
	logicPortType[2] = 0;
	logicPortPos[2] = "-1 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "Out1";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 0 0";
	logicPortDir[3] = 2;
	logicPortUIName[3] = "Enable";
	logicPortCauseUpdate[3] = true;
	
};
