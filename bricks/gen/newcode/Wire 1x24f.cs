
datablock fxDtsBrickData(LogicWire_1x24f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x24f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x24f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x24f";
	
	logicBrickSize = "1 24 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
