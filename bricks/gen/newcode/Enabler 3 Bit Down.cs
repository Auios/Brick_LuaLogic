
datablock fxDtsBrickData(LogicGate_Enabler3BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Enabler 3 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Enabler 3 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Enabler 3 Bit Down";
	logicUIName = "Enabler 3 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "3 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 7)~=0 then " @
		"		Gate.setportstate(gate, 4, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 5, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 6, Gate.getportstate(gate, 3)) " @
		"	else " @
		"		Gate.setportstate(gate, 4, 0) " @
		"		Gate.setportstate(gate, 5, 0) " @
		"		Gate.setportstate(gate, 6, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 7;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "2 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-2 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 0;
	logicPortPos[3] = "2 0 0";
	logicPortDir[3] = 5;
	logicPortUIName[3] = "Out0";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "0 0 0";
	logicPortDir[4] = 5;
	logicPortUIName[4] = "Out1";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "-2 0 0";
	logicPortDir[5] = 5;
	logicPortUIName[5] = "Out2";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "2 0 0";
	logicPortDir[6] = 2;
	logicPortUIName[6] = "Clock";
	logicPortCauseUpdate[6] = true;
	
};
