
datablock fxDtsBrickData(LogicWire_1x1x12_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x12.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x12";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x12";
	
	logicBrickSize = "1 1 36";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
