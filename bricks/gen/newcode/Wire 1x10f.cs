
datablock fxDtsBrickData(LogicWire_1x10f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x10f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x10f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x10f";
	
	logicBrickSize = "1 10 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
