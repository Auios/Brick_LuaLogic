
datablock fxDtsBrickData(LogicWire_1x42f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x42f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x42f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x42f";
	
	logicBrickSize = "1 42 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
