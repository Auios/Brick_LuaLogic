
datablock fxDtsBrickData(LogicGate_Rom64x32x48_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 64x32x48.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 64x32x48";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 64x32x48";
	logicUIName = "ROM 64x32x48";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 32 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 98303 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 60;
	
	isLogicRom = true;
	logicRomY = 32;
	logicRomZ = 48;
	logicRomX = 64;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -31 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -31 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -31 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -31 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -31 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -31 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 -31 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 -31 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 -31 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 -31 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 -31 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "A10";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "63 31 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O0";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "61 31 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O1";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "59 31 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O2";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "57 31 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O3";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "55 31 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O4";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "53 31 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O5";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "51 31 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O6";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "49 31 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O7";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "47 31 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O8";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "45 31 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O9";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "43 31 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O10";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "41 31 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O11";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "39 31 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O12";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "37 31 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O13";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "35 31 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O14";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "33 31 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O15";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "31 31 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O16";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "29 31 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O17";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "27 31 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O18";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "25 31 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O19";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "23 31 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O20";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "21 31 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O21";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "19 31 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O22";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "17 31 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O23";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "15 31 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O24";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "13 31 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O25";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "11 31 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O26";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "9 31 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O27";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "7 31 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O28";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "5 31 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O29";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "3 31 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "O30";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "1 31 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "O31";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "-1 31 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "O32";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "-3 31 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "O33";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "-5 31 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "O34";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "-7 31 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "O35";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "-9 31 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "O36";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-11 31 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "O37";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-13 31 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "O38";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-15 31 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "O39";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-17 31 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "O40";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-19 31 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "O41";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-21 31 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "O42";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-23 31 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "O43";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-25 31 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "O44";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-27 31 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "O45";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-29 31 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "O46";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-31 31 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "O47";
	
	logicPortType[59] = 1;
	logicPortPos[59] = "63 -31 0";
	logicPortDir[59] = 2;
	logicPortUIName[59] = "Clock";
	logicPortCauseUpdate[59] = true;
	
};
