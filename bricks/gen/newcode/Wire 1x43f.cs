
datablock fxDtsBrickData(LogicWire_1x43f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x43f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x43f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x43f";
	
	logicBrickSize = "1 43 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
