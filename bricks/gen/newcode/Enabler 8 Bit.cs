
datablock fxDtsBrickData(LogicGate_Enabler8Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Enabler 8 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Enabler 8 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Enabler 8 Bit";
	logicUIName = "Enabler 8 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "8 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 17)~=0 then " @
		"		Gate.setportstate(gate, 9, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 10, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 11, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 12, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 13, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 14, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 15, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 16, Gate.getportstate(gate, 8)) " @
		"	else " @
		"		Gate.setportstate(gate, 9, 0) " @
		"		Gate.setportstate(gate, 10, 0) " @
		"		Gate.setportstate(gate, 11, 0) " @
		"		Gate.setportstate(gate, 12, 0) " @
		"		Gate.setportstate(gate, 13, 0) " @
		"		Gate.setportstate(gate, 14, 0) " @
		"		Gate.setportstate(gate, 15, 0) " @
		"		Gate.setportstate(gate, 16, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 17;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "7 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "5 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-1 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-3 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-5 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-7 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 0;
	logicPortPos[8] = "7 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out0";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "5 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out1";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "3 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out2";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "1 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out3";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "-1 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out4";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "-3 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out5";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "-5 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out6";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "-7 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out7";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "7 0 0";
	logicPortDir[16] = 2;
	logicPortUIName[16] = "Clock";
	logicPortCauseUpdate[16] = true;
	
};
