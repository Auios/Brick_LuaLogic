
datablock fxDtsBrickData(LogicGate_Ram8x8_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/RAM 256 B.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/RAM 256 B";
	
	category = "Logic Bricks";
	subCategory = "RAM";
	uiName = "RAM 256 B";
	logicUIName = "RAM 256 B";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "8 8 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 255 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = "";
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 26;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "7 -7 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "I0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "5 -7 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "I1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 -7 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "I2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 -7 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "I3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-1 -7 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "I4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-3 -7 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "I5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-5 -7 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "I6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-7 -7 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "I7";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "7 7 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O0";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "5 7 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O1";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "3 7 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O2";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "1 7 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O3";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "-1 7 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O4";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "-3 7 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O5";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "-5 7 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O6";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "-7 7 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O7";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "-7 -7 0";
	logicPortDir[16] = 0;
	logicPortUIName[16] = "A0";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "-7 -5 0";
	logicPortDir[17] = 0;
	logicPortUIName[17] = "A1";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "-7 -3 0";
	logicPortDir[18] = 0;
	logicPortUIName[18] = "A2";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "-7 -1 0";
	logicPortDir[19] = 0;
	logicPortUIName[19] = "A3";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "-7 1 0";
	logicPortDir[20] = 0;
	logicPortUIName[20] = "A4";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "-7 3 0";
	logicPortDir[21] = 0;
	logicPortUIName[21] = "A5";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "-7 5 0";
	logicPortDir[22] = 0;
	logicPortUIName[22] = "A6";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "-7 7 0";
	logicPortDir[23] = 0;
	logicPortUIName[23] = "A7";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "7 7 0";
	logicPortDir[24] = 2;
	logicPortUIName[24] = "Read";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 1;
	logicPortPos[25] = "7 -7 0";
	logicPortDir[25] = 2;
	logicPortUIName[25] = "Write";
	logicPortCauseUpdate[25] = true;
	
};
