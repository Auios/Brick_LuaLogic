
datablock fxDtsBrickData(LogicGate_Mux4_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 4 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 4 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 4 Bit";
	logicUIName = "Mux 4 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "16 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 21) then " @
		"		local idx = 5 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) " @
		"		Gate.setportstate(gate, 22, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 22, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 22;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "15 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "13 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "11 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "9 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "15 0 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "In0";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "13 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "In1";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "11 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "In2";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "9 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "In3";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "7 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "In4";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "5 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "In5";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "3 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "In6";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "1 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "In7";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "-1 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "In8";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "-3 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "In9";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "-5 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "In10";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "-7 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "In11";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "-9 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "In12";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "-11 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "In13";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "-13 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "In14";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "-15 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "In15";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "15 0 0";
	logicPortDir[20] = 2;
	logicPortUIName[20] = "Enable";
	logicPortCauseUpdate[20] = true;
	
	logicPortType[21] = 0;
	logicPortPos[21] = "-15 0 0";
	logicPortDir[21] = 0;
	logicPortUIName[21] = "Out";
	
};
