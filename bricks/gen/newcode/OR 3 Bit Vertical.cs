
datablock fxDtsBrickData(LogicGate_GateOr3Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/OR 3 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/OR 3 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "OR 3 Bit Vertical";
	logicUIName = "OR 3 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 3";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 4, (( " @
		"		(Gate.getportstate(gate, 1)~=0) or " @
		"		(Gate.getportstate(gate, 2)~=0) or " @
		"		(Gate.getportstate(gate, 3)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 4;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 2";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -2";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 0;
	logicPortPos[3] = "0 0 -2";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "Out";
	
};
