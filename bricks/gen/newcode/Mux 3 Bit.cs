
datablock fxDtsBrickData(LogicGate_Mux3_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 3 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 3 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 3 Bit";
	logicUIName = "Mux 3 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "8 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 12) then " @
		"		local idx = 4 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) " @
		"		Gate.setportstate(gate, 13, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 13, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 13;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "7 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "5 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "7 0 0";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "In0";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "5 0 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "In1";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "3 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "In2";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "1 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "In3";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-1 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "In4";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-3 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "In5";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "-5 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "In6";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "-7 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "In7";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "7 0 0";
	logicPortDir[11] = 2;
	logicPortUIName[11] = "Enable";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 0;
	logicPortPos[12] = "-7 0 0";
	logicPortDir[12] = 0;
	logicPortUIName[12] = "Out";
	
};
