
datablock fxDtsBrickData(LogicGate_DFlipFlop48Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 48 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 48 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 48 Bit";
	logicUIName = "D FlipFlop 48 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "48 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 97)~=0 then " @
		"		Gate.setportstate(gate, 49, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 50, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 51, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 52, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 53, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 54, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 55, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 56, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 57, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 58, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 59, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 60, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 61, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 62, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 63, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 64, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 65, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 66, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 67, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 68, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 69, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 70, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 71, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 72, Gate.getportstate(gate, 24)) " @
		"		Gate.setportstate(gate, 73, Gate.getportstate(gate, 25)) " @
		"		Gate.setportstate(gate, 74, Gate.getportstate(gate, 26)) " @
		"		Gate.setportstate(gate, 75, Gate.getportstate(gate, 27)) " @
		"		Gate.setportstate(gate, 76, Gate.getportstate(gate, 28)) " @
		"		Gate.setportstate(gate, 77, Gate.getportstate(gate, 29)) " @
		"		Gate.setportstate(gate, 78, Gate.getportstate(gate, 30)) " @
		"		Gate.setportstate(gate, 79, Gate.getportstate(gate, 31)) " @
		"		Gate.setportstate(gate, 80, Gate.getportstate(gate, 32)) " @
		"		Gate.setportstate(gate, 81, Gate.getportstate(gate, 33)) " @
		"		Gate.setportstate(gate, 82, Gate.getportstate(gate, 34)) " @
		"		Gate.setportstate(gate, 83, Gate.getportstate(gate, 35)) " @
		"		Gate.setportstate(gate, 84, Gate.getportstate(gate, 36)) " @
		"		Gate.setportstate(gate, 85, Gate.getportstate(gate, 37)) " @
		"		Gate.setportstate(gate, 86, Gate.getportstate(gate, 38)) " @
		"		Gate.setportstate(gate, 87, Gate.getportstate(gate, 39)) " @
		"		Gate.setportstate(gate, 88, Gate.getportstate(gate, 40)) " @
		"		Gate.setportstate(gate, 89, Gate.getportstate(gate, 41)) " @
		"		Gate.setportstate(gate, 90, Gate.getportstate(gate, 42)) " @
		"		Gate.setportstate(gate, 91, Gate.getportstate(gate, 43)) " @
		"		Gate.setportstate(gate, 92, Gate.getportstate(gate, 44)) " @
		"		Gate.setportstate(gate, 93, Gate.getportstate(gate, 45)) " @
		"		Gate.setportstate(gate, 94, Gate.getportstate(gate, 46)) " @
		"		Gate.setportstate(gate, 95, Gate.getportstate(gate, 47)) " @
		"		Gate.setportstate(gate, 96, Gate.getportstate(gate, 48)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 97;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "47 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "45 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "43 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "41 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "39 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "37 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "35 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "33 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "31 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "29 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "27 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "25 0 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "23 0 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "In12";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "21 0 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "In13";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "19 0 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "In14";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "17 0 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "In15";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "15 0 0";
	logicPortDir[16] = 3;
	logicPortUIName[16] = "In16";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "13 0 0";
	logicPortDir[17] = 3;
	logicPortUIName[17] = "In17";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "11 0 0";
	logicPortDir[18] = 3;
	logicPortUIName[18] = "In18";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "9 0 0";
	logicPortDir[19] = 3;
	logicPortUIName[19] = "In19";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "7 0 0";
	logicPortDir[20] = 3;
	logicPortUIName[20] = "In20";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "5 0 0";
	logicPortDir[21] = 3;
	logicPortUIName[21] = "In21";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "3 0 0";
	logicPortDir[22] = 3;
	logicPortUIName[22] = "In22";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "1 0 0";
	logicPortDir[23] = 3;
	logicPortUIName[23] = "In23";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "-1 0 0";
	logicPortDir[24] = 3;
	logicPortUIName[24] = "In24";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "-3 0 0";
	logicPortDir[25] = 3;
	logicPortUIName[25] = "In25";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "-5 0 0";
	logicPortDir[26] = 3;
	logicPortUIName[26] = "In26";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "-7 0 0";
	logicPortDir[27] = 3;
	logicPortUIName[27] = "In27";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "-9 0 0";
	logicPortDir[28] = 3;
	logicPortUIName[28] = "In28";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "-11 0 0";
	logicPortDir[29] = 3;
	logicPortUIName[29] = "In29";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "-13 0 0";
	logicPortDir[30] = 3;
	logicPortUIName[30] = "In30";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "-15 0 0";
	logicPortDir[31] = 3;
	logicPortUIName[31] = "In31";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "-17 0 0";
	logicPortDir[32] = 3;
	logicPortUIName[32] = "In32";
	
	logicPortType[33] = 1;
	logicPortPos[33] = "-19 0 0";
	logicPortDir[33] = 3;
	logicPortUIName[33] = "In33";
	
	logicPortType[34] = 1;
	logicPortPos[34] = "-21 0 0";
	logicPortDir[34] = 3;
	logicPortUIName[34] = "In34";
	
	logicPortType[35] = 1;
	logicPortPos[35] = "-23 0 0";
	logicPortDir[35] = 3;
	logicPortUIName[35] = "In35";
	
	logicPortType[36] = 1;
	logicPortPos[36] = "-25 0 0";
	logicPortDir[36] = 3;
	logicPortUIName[36] = "In36";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "-27 0 0";
	logicPortDir[37] = 3;
	logicPortUIName[37] = "In37";
	
	logicPortType[38] = 1;
	logicPortPos[38] = "-29 0 0";
	logicPortDir[38] = 3;
	logicPortUIName[38] = "In38";
	
	logicPortType[39] = 1;
	logicPortPos[39] = "-31 0 0";
	logicPortDir[39] = 3;
	logicPortUIName[39] = "In39";
	
	logicPortType[40] = 1;
	logicPortPos[40] = "-33 0 0";
	logicPortDir[40] = 3;
	logicPortUIName[40] = "In40";
	
	logicPortType[41] = 1;
	logicPortPos[41] = "-35 0 0";
	logicPortDir[41] = 3;
	logicPortUIName[41] = "In41";
	
	logicPortType[42] = 1;
	logicPortPos[42] = "-37 0 0";
	logicPortDir[42] = 3;
	logicPortUIName[42] = "In42";
	
	logicPortType[43] = 1;
	logicPortPos[43] = "-39 0 0";
	logicPortDir[43] = 3;
	logicPortUIName[43] = "In43";
	
	logicPortType[44] = 1;
	logicPortPos[44] = "-41 0 0";
	logicPortDir[44] = 3;
	logicPortUIName[44] = "In44";
	
	logicPortType[45] = 1;
	logicPortPos[45] = "-43 0 0";
	logicPortDir[45] = 3;
	logicPortUIName[45] = "In45";
	
	logicPortType[46] = 1;
	logicPortPos[46] = "-45 0 0";
	logicPortDir[46] = 3;
	logicPortUIName[46] = "In46";
	
	logicPortType[47] = 1;
	logicPortPos[47] = "-47 0 0";
	logicPortDir[47] = 3;
	logicPortUIName[47] = "In47";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "47 0 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "Out0";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "45 0 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "Out1";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "43 0 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "Out2";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "41 0 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "Out3";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "39 0 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "Out4";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "37 0 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "Out5";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "35 0 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "Out6";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "33 0 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "Out7";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "31 0 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "Out8";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "29 0 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "Out9";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "27 0 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "Out10";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "25 0 0";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "Out11";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "23 0 0";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "Out12";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "21 0 0";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "Out13";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "19 0 0";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "Out14";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "17 0 0";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "Out15";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "15 0 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "Out16";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "13 0 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "Out17";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "11 0 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "Out18";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "9 0 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "Out19";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "7 0 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "Out20";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "5 0 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "Out21";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "3 0 0";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "Out22";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "1 0 0";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "Out23";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "-1 0 0";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "Out24";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "-3 0 0";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "Out25";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "-5 0 0";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "Out26";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "-7 0 0";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "Out27";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "-9 0 0";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "Out28";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "-11 0 0";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "Out29";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "-13 0 0";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "Out30";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "-15 0 0";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "Out31";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "-17 0 0";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "Out32";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "-19 0 0";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "Out33";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "-21 0 0";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "Out34";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "-23 0 0";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "Out35";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "-25 0 0";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "Out36";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "-27 0 0";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "Out37";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "-29 0 0";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "Out38";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "-31 0 0";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "Out39";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "-33 0 0";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "Out40";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "-35 0 0";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "Out41";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "-37 0 0";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "Out42";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "-39 0 0";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "Out43";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "-41 0 0";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "Out44";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "-43 0 0";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "Out45";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "-45 0 0";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "Out46";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "-47 0 0";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "Out47";
	
	logicPortType[96] = 1;
	logicPortPos[96] = "47 0 0";
	logicPortDir[96] = 2;
	logicPortUIName[96] = "Clock";
	logicPortCauseUpdate[96] = true;
	
};
