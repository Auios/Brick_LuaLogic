
datablock fxDtsBrickData(LogicWire_1x1x15_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x15.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x15";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x15";
	
	logicBrickSize = "1 1 45";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
