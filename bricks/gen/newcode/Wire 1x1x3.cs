
datablock fxDtsBrickData(LogicWire_1x1x3_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x3.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x3";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x3";
	
	logicBrickSize = "1 1 9";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
