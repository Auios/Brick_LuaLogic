
datablock fxDtsBrickData(LogicWire_1x1x11_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x11.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x11";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x11";
	
	logicBrickSize = "1 1 33";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
