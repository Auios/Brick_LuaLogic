
datablock fxDtsBrickData(LogicWire_1x1x35f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x35f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x35f";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x35f";
	
	logicBrickSize = "1 1 35";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
