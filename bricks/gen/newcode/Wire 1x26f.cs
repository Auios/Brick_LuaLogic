
datablock fxDtsBrickData(LogicWire_1x26f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x26f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x26f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x26f";
	
	logicBrickSize = "1 26 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
