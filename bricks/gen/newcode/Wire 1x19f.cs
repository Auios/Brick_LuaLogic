
datablock fxDtsBrickData(LogicWire_1x19f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x19f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x19f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x19f";
	
	logicBrickSize = "1 19 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
