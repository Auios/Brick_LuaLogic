
datablock fxDtsBrickData(LogicGate_DFlipFlop11Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 11 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 11 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 11 Bit";
	logicUIName = "D FlipFlop 11 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "11 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 23)~=0 then " @
		"		Gate.setportstate(gate, 12, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 13, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 14, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 15, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 16, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 17, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 18, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 19, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 20, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 21, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 22, Gate.getportstate(gate, 11)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 23;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "10 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "8 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "6 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "4 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "2 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-2 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-4 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-6 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "-8 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "-10 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "10 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out0";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "8 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out1";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "6 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out2";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "4 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out3";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "2 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out4";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "0 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out5";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "-2 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out6";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "-4 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out7";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "-6 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out8";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "-8 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out9";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "-10 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out10";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "10 0 0";
	logicPortDir[22] = 2;
	logicPortUIName[22] = "Clock";
	logicPortCauseUpdate[22] = true;
	
};
