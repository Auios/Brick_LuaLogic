
datablock fxDtsBrickData(LogicWire_1x54f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x54f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x54f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x54f";
	
	logicBrickSize = "1 54 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
