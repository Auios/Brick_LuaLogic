
datablock fxDtsBrickData(LogicGate_Demux8Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 8 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 8 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 8 Bit Vertical";
	logicUIName = "Demux 8 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 256";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 9 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 265)~=0 then " @
		"		local idx = 9 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) + " @
		"			(Gate.getportstate(gate, 6) * 32) + " @
		"			(Gate.getportstate(gate, 7) * 64) + " @
		"			(Gate.getportstate(gate, 8) * 128) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 265;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -255";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -253";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -251";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -249";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -247";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -245";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "Sel5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 0 -243";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "Sel6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 0 -241";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "Sel7";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "0 0 -255";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out0";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "0 0 -253";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out1";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "0 0 -251";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out2";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "0 0 -249";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out3";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "0 0 -247";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out4";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "0 0 -245";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out5";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "0 0 -243";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out6";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "0 0 -241";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out7";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "0 0 -239";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out8";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "0 0 -237";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out9";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "0 0 -235";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out10";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "0 0 -233";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out11";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "0 0 -231";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out12";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "0 0 -229";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out13";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "0 0 -227";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out14";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "0 0 -225";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out15";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "0 0 -223";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out16";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "0 0 -221";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out17";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "0 0 -219";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out18";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "0 0 -217";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out19";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "0 0 -215";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out20";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "0 0 -213";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out21";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "0 0 -211";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "Out22";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "0 0 -209";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "Out23";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "0 0 -207";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "Out24";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "0 0 -205";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "Out25";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "0 0 -203";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "Out26";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "0 0 -201";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "Out27";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "0 0 -199";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "Out28";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "0 0 -197";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "Out29";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "0 0 -195";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "Out30";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "0 0 -193";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "Out31";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "0 0 -191";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "Out32";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "0 0 -189";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "Out33";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "0 0 -187";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "Out34";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "0 0 -185";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "Out35";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "0 0 -183";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "Out36";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "0 0 -181";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "Out37";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "0 0 -179";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "Out38";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "0 0 -177";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "Out39";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "0 0 -175";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "Out40";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "0 0 -173";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "Out41";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "0 0 -171";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "Out42";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "0 0 -169";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "Out43";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "0 0 -167";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "Out44";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "0 0 -165";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "Out45";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "0 0 -163";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "Out46";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "0 0 -161";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "Out47";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "0 0 -159";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "Out48";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "0 0 -157";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "Out49";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "0 0 -155";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "Out50";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "0 0 -153";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "Out51";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "0 0 -151";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "Out52";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "0 0 -149";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "Out53";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "0 0 -147";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "Out54";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "0 0 -145";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "Out55";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "0 0 -143";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "Out56";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "0 0 -141";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "Out57";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "0 0 -139";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "Out58";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "0 0 -137";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "Out59";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "0 0 -135";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "Out60";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "0 0 -133";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "Out61";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "0 0 -131";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "Out62";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "0 0 -129";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "Out63";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "0 0 -127";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "Out64";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "0 0 -125";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "Out65";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "0 0 -123";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "Out66";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "0 0 -121";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "Out67";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "0 0 -119";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "Out68";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "0 0 -117";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "Out69";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "0 0 -115";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "Out70";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "0 0 -113";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "Out71";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "0 0 -111";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "Out72";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "0 0 -109";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "Out73";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "0 0 -107";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "Out74";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "0 0 -105";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "Out75";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "0 0 -103";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "Out76";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "0 0 -101";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "Out77";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "0 0 -99";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "Out78";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "0 0 -97";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "Out79";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "0 0 -95";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "Out80";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "0 0 -93";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "Out81";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "0 0 -91";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "Out82";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "0 0 -89";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "Out83";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "0 0 -87";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "Out84";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "0 0 -85";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "Out85";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "0 0 -83";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "Out86";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "0 0 -81";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "Out87";
	
	logicPortType[96] = 0;
	logicPortPos[96] = "0 0 -79";
	logicPortDir[96] = 1;
	logicPortUIName[96] = "Out88";
	
	logicPortType[97] = 0;
	logicPortPos[97] = "0 0 -77";
	logicPortDir[97] = 1;
	logicPortUIName[97] = "Out89";
	
	logicPortType[98] = 0;
	logicPortPos[98] = "0 0 -75";
	logicPortDir[98] = 1;
	logicPortUIName[98] = "Out90";
	
	logicPortType[99] = 0;
	logicPortPos[99] = "0 0 -73";
	logicPortDir[99] = 1;
	logicPortUIName[99] = "Out91";
	
	logicPortType[100] = 0;
	logicPortPos[100] = "0 0 -71";
	logicPortDir[100] = 1;
	logicPortUIName[100] = "Out92";
	
	logicPortType[101] = 0;
	logicPortPos[101] = "0 0 -69";
	logicPortDir[101] = 1;
	logicPortUIName[101] = "Out93";
	
	logicPortType[102] = 0;
	logicPortPos[102] = "0 0 -67";
	logicPortDir[102] = 1;
	logicPortUIName[102] = "Out94";
	
	logicPortType[103] = 0;
	logicPortPos[103] = "0 0 -65";
	logicPortDir[103] = 1;
	logicPortUIName[103] = "Out95";
	
	logicPortType[104] = 0;
	logicPortPos[104] = "0 0 -63";
	logicPortDir[104] = 1;
	logicPortUIName[104] = "Out96";
	
	logicPortType[105] = 0;
	logicPortPos[105] = "0 0 -61";
	logicPortDir[105] = 1;
	logicPortUIName[105] = "Out97";
	
	logicPortType[106] = 0;
	logicPortPos[106] = "0 0 -59";
	logicPortDir[106] = 1;
	logicPortUIName[106] = "Out98";
	
	logicPortType[107] = 0;
	logicPortPos[107] = "0 0 -57";
	logicPortDir[107] = 1;
	logicPortUIName[107] = "Out99";
	
	logicPortType[108] = 0;
	logicPortPos[108] = "0 0 -55";
	logicPortDir[108] = 1;
	logicPortUIName[108] = "Out100";
	
	logicPortType[109] = 0;
	logicPortPos[109] = "0 0 -53";
	logicPortDir[109] = 1;
	logicPortUIName[109] = "Out101";
	
	logicPortType[110] = 0;
	logicPortPos[110] = "0 0 -51";
	logicPortDir[110] = 1;
	logicPortUIName[110] = "Out102";
	
	logicPortType[111] = 0;
	logicPortPos[111] = "0 0 -49";
	logicPortDir[111] = 1;
	logicPortUIName[111] = "Out103";
	
	logicPortType[112] = 0;
	logicPortPos[112] = "0 0 -47";
	logicPortDir[112] = 1;
	logicPortUIName[112] = "Out104";
	
	logicPortType[113] = 0;
	logicPortPos[113] = "0 0 -45";
	logicPortDir[113] = 1;
	logicPortUIName[113] = "Out105";
	
	logicPortType[114] = 0;
	logicPortPos[114] = "0 0 -43";
	logicPortDir[114] = 1;
	logicPortUIName[114] = "Out106";
	
	logicPortType[115] = 0;
	logicPortPos[115] = "0 0 -41";
	logicPortDir[115] = 1;
	logicPortUIName[115] = "Out107";
	
	logicPortType[116] = 0;
	logicPortPos[116] = "0 0 -39";
	logicPortDir[116] = 1;
	logicPortUIName[116] = "Out108";
	
	logicPortType[117] = 0;
	logicPortPos[117] = "0 0 -37";
	logicPortDir[117] = 1;
	logicPortUIName[117] = "Out109";
	
	logicPortType[118] = 0;
	logicPortPos[118] = "0 0 -35";
	logicPortDir[118] = 1;
	logicPortUIName[118] = "Out110";
	
	logicPortType[119] = 0;
	logicPortPos[119] = "0 0 -33";
	logicPortDir[119] = 1;
	logicPortUIName[119] = "Out111";
	
	logicPortType[120] = 0;
	logicPortPos[120] = "0 0 -31";
	logicPortDir[120] = 1;
	logicPortUIName[120] = "Out112";
	
	logicPortType[121] = 0;
	logicPortPos[121] = "0 0 -29";
	logicPortDir[121] = 1;
	logicPortUIName[121] = "Out113";
	
	logicPortType[122] = 0;
	logicPortPos[122] = "0 0 -27";
	logicPortDir[122] = 1;
	logicPortUIName[122] = "Out114";
	
	logicPortType[123] = 0;
	logicPortPos[123] = "0 0 -25";
	logicPortDir[123] = 1;
	logicPortUIName[123] = "Out115";
	
	logicPortType[124] = 0;
	logicPortPos[124] = "0 0 -23";
	logicPortDir[124] = 1;
	logicPortUIName[124] = "Out116";
	
	logicPortType[125] = 0;
	logicPortPos[125] = "0 0 -21";
	logicPortDir[125] = 1;
	logicPortUIName[125] = "Out117";
	
	logicPortType[126] = 0;
	logicPortPos[126] = "0 0 -19";
	logicPortDir[126] = 1;
	logicPortUIName[126] = "Out118";
	
	logicPortType[127] = 0;
	logicPortPos[127] = "0 0 -17";
	logicPortDir[127] = 1;
	logicPortUIName[127] = "Out119";
	
	logicPortType[128] = 0;
	logicPortPos[128] = "0 0 -15";
	logicPortDir[128] = 1;
	logicPortUIName[128] = "Out120";
	
	logicPortType[129] = 0;
	logicPortPos[129] = "0 0 -13";
	logicPortDir[129] = 1;
	logicPortUIName[129] = "Out121";
	
	logicPortType[130] = 0;
	logicPortPos[130] = "0 0 -11";
	logicPortDir[130] = 1;
	logicPortUIName[130] = "Out122";
	
	logicPortType[131] = 0;
	logicPortPos[131] = "0 0 -9";
	logicPortDir[131] = 1;
	logicPortUIName[131] = "Out123";
	
	logicPortType[132] = 0;
	logicPortPos[132] = "0 0 -7";
	logicPortDir[132] = 1;
	logicPortUIName[132] = "Out124";
	
	logicPortType[133] = 0;
	logicPortPos[133] = "0 0 -5";
	logicPortDir[133] = 1;
	logicPortUIName[133] = "Out125";
	
	logicPortType[134] = 0;
	logicPortPos[134] = "0 0 -3";
	logicPortDir[134] = 1;
	logicPortUIName[134] = "Out126";
	
	logicPortType[135] = 0;
	logicPortPos[135] = "0 0 -1";
	logicPortDir[135] = 1;
	logicPortUIName[135] = "Out127";
	
	logicPortType[136] = 0;
	logicPortPos[136] = "0 0 1";
	logicPortDir[136] = 1;
	logicPortUIName[136] = "Out128";
	
	logicPortType[137] = 0;
	logicPortPos[137] = "0 0 3";
	logicPortDir[137] = 1;
	logicPortUIName[137] = "Out129";
	
	logicPortType[138] = 0;
	logicPortPos[138] = "0 0 5";
	logicPortDir[138] = 1;
	logicPortUIName[138] = "Out130";
	
	logicPortType[139] = 0;
	logicPortPos[139] = "0 0 7";
	logicPortDir[139] = 1;
	logicPortUIName[139] = "Out131";
	
	logicPortType[140] = 0;
	logicPortPos[140] = "0 0 9";
	logicPortDir[140] = 1;
	logicPortUIName[140] = "Out132";
	
	logicPortType[141] = 0;
	logicPortPos[141] = "0 0 11";
	logicPortDir[141] = 1;
	logicPortUIName[141] = "Out133";
	
	logicPortType[142] = 0;
	logicPortPos[142] = "0 0 13";
	logicPortDir[142] = 1;
	logicPortUIName[142] = "Out134";
	
	logicPortType[143] = 0;
	logicPortPos[143] = "0 0 15";
	logicPortDir[143] = 1;
	logicPortUIName[143] = "Out135";
	
	logicPortType[144] = 0;
	logicPortPos[144] = "0 0 17";
	logicPortDir[144] = 1;
	logicPortUIName[144] = "Out136";
	
	logicPortType[145] = 0;
	logicPortPos[145] = "0 0 19";
	logicPortDir[145] = 1;
	logicPortUIName[145] = "Out137";
	
	logicPortType[146] = 0;
	logicPortPos[146] = "0 0 21";
	logicPortDir[146] = 1;
	logicPortUIName[146] = "Out138";
	
	logicPortType[147] = 0;
	logicPortPos[147] = "0 0 23";
	logicPortDir[147] = 1;
	logicPortUIName[147] = "Out139";
	
	logicPortType[148] = 0;
	logicPortPos[148] = "0 0 25";
	logicPortDir[148] = 1;
	logicPortUIName[148] = "Out140";
	
	logicPortType[149] = 0;
	logicPortPos[149] = "0 0 27";
	logicPortDir[149] = 1;
	logicPortUIName[149] = "Out141";
	
	logicPortType[150] = 0;
	logicPortPos[150] = "0 0 29";
	logicPortDir[150] = 1;
	logicPortUIName[150] = "Out142";
	
	logicPortType[151] = 0;
	logicPortPos[151] = "0 0 31";
	logicPortDir[151] = 1;
	logicPortUIName[151] = "Out143";
	
	logicPortType[152] = 0;
	logicPortPos[152] = "0 0 33";
	logicPortDir[152] = 1;
	logicPortUIName[152] = "Out144";
	
	logicPortType[153] = 0;
	logicPortPos[153] = "0 0 35";
	logicPortDir[153] = 1;
	logicPortUIName[153] = "Out145";
	
	logicPortType[154] = 0;
	logicPortPos[154] = "0 0 37";
	logicPortDir[154] = 1;
	logicPortUIName[154] = "Out146";
	
	logicPortType[155] = 0;
	logicPortPos[155] = "0 0 39";
	logicPortDir[155] = 1;
	logicPortUIName[155] = "Out147";
	
	logicPortType[156] = 0;
	logicPortPos[156] = "0 0 41";
	logicPortDir[156] = 1;
	logicPortUIName[156] = "Out148";
	
	logicPortType[157] = 0;
	logicPortPos[157] = "0 0 43";
	logicPortDir[157] = 1;
	logicPortUIName[157] = "Out149";
	
	logicPortType[158] = 0;
	logicPortPos[158] = "0 0 45";
	logicPortDir[158] = 1;
	logicPortUIName[158] = "Out150";
	
	logicPortType[159] = 0;
	logicPortPos[159] = "0 0 47";
	logicPortDir[159] = 1;
	logicPortUIName[159] = "Out151";
	
	logicPortType[160] = 0;
	logicPortPos[160] = "0 0 49";
	logicPortDir[160] = 1;
	logicPortUIName[160] = "Out152";
	
	logicPortType[161] = 0;
	logicPortPos[161] = "0 0 51";
	logicPortDir[161] = 1;
	logicPortUIName[161] = "Out153";
	
	logicPortType[162] = 0;
	logicPortPos[162] = "0 0 53";
	logicPortDir[162] = 1;
	logicPortUIName[162] = "Out154";
	
	logicPortType[163] = 0;
	logicPortPos[163] = "0 0 55";
	logicPortDir[163] = 1;
	logicPortUIName[163] = "Out155";
	
	logicPortType[164] = 0;
	logicPortPos[164] = "0 0 57";
	logicPortDir[164] = 1;
	logicPortUIName[164] = "Out156";
	
	logicPortType[165] = 0;
	logicPortPos[165] = "0 0 59";
	logicPortDir[165] = 1;
	logicPortUIName[165] = "Out157";
	
	logicPortType[166] = 0;
	logicPortPos[166] = "0 0 61";
	logicPortDir[166] = 1;
	logicPortUIName[166] = "Out158";
	
	logicPortType[167] = 0;
	logicPortPos[167] = "0 0 63";
	logicPortDir[167] = 1;
	logicPortUIName[167] = "Out159";
	
	logicPortType[168] = 0;
	logicPortPos[168] = "0 0 65";
	logicPortDir[168] = 1;
	logicPortUIName[168] = "Out160";
	
	logicPortType[169] = 0;
	logicPortPos[169] = "0 0 67";
	logicPortDir[169] = 1;
	logicPortUIName[169] = "Out161";
	
	logicPortType[170] = 0;
	logicPortPos[170] = "0 0 69";
	logicPortDir[170] = 1;
	logicPortUIName[170] = "Out162";
	
	logicPortType[171] = 0;
	logicPortPos[171] = "0 0 71";
	logicPortDir[171] = 1;
	logicPortUIName[171] = "Out163";
	
	logicPortType[172] = 0;
	logicPortPos[172] = "0 0 73";
	logicPortDir[172] = 1;
	logicPortUIName[172] = "Out164";
	
	logicPortType[173] = 0;
	logicPortPos[173] = "0 0 75";
	logicPortDir[173] = 1;
	logicPortUIName[173] = "Out165";
	
	logicPortType[174] = 0;
	logicPortPos[174] = "0 0 77";
	logicPortDir[174] = 1;
	logicPortUIName[174] = "Out166";
	
	logicPortType[175] = 0;
	logicPortPos[175] = "0 0 79";
	logicPortDir[175] = 1;
	logicPortUIName[175] = "Out167";
	
	logicPortType[176] = 0;
	logicPortPos[176] = "0 0 81";
	logicPortDir[176] = 1;
	logicPortUIName[176] = "Out168";
	
	logicPortType[177] = 0;
	logicPortPos[177] = "0 0 83";
	logicPortDir[177] = 1;
	logicPortUIName[177] = "Out169";
	
	logicPortType[178] = 0;
	logicPortPos[178] = "0 0 85";
	logicPortDir[178] = 1;
	logicPortUIName[178] = "Out170";
	
	logicPortType[179] = 0;
	logicPortPos[179] = "0 0 87";
	logicPortDir[179] = 1;
	logicPortUIName[179] = "Out171";
	
	logicPortType[180] = 0;
	logicPortPos[180] = "0 0 89";
	logicPortDir[180] = 1;
	logicPortUIName[180] = "Out172";
	
	logicPortType[181] = 0;
	logicPortPos[181] = "0 0 91";
	logicPortDir[181] = 1;
	logicPortUIName[181] = "Out173";
	
	logicPortType[182] = 0;
	logicPortPos[182] = "0 0 93";
	logicPortDir[182] = 1;
	logicPortUIName[182] = "Out174";
	
	logicPortType[183] = 0;
	logicPortPos[183] = "0 0 95";
	logicPortDir[183] = 1;
	logicPortUIName[183] = "Out175";
	
	logicPortType[184] = 0;
	logicPortPos[184] = "0 0 97";
	logicPortDir[184] = 1;
	logicPortUIName[184] = "Out176";
	
	logicPortType[185] = 0;
	logicPortPos[185] = "0 0 99";
	logicPortDir[185] = 1;
	logicPortUIName[185] = "Out177";
	
	logicPortType[186] = 0;
	logicPortPos[186] = "0 0 101";
	logicPortDir[186] = 1;
	logicPortUIName[186] = "Out178";
	
	logicPortType[187] = 0;
	logicPortPos[187] = "0 0 103";
	logicPortDir[187] = 1;
	logicPortUIName[187] = "Out179";
	
	logicPortType[188] = 0;
	logicPortPos[188] = "0 0 105";
	logicPortDir[188] = 1;
	logicPortUIName[188] = "Out180";
	
	logicPortType[189] = 0;
	logicPortPos[189] = "0 0 107";
	logicPortDir[189] = 1;
	logicPortUIName[189] = "Out181";
	
	logicPortType[190] = 0;
	logicPortPos[190] = "0 0 109";
	logicPortDir[190] = 1;
	logicPortUIName[190] = "Out182";
	
	logicPortType[191] = 0;
	logicPortPos[191] = "0 0 111";
	logicPortDir[191] = 1;
	logicPortUIName[191] = "Out183";
	
	logicPortType[192] = 0;
	logicPortPos[192] = "0 0 113";
	logicPortDir[192] = 1;
	logicPortUIName[192] = "Out184";
	
	logicPortType[193] = 0;
	logicPortPos[193] = "0 0 115";
	logicPortDir[193] = 1;
	logicPortUIName[193] = "Out185";
	
	logicPortType[194] = 0;
	logicPortPos[194] = "0 0 117";
	logicPortDir[194] = 1;
	logicPortUIName[194] = "Out186";
	
	logicPortType[195] = 0;
	logicPortPos[195] = "0 0 119";
	logicPortDir[195] = 1;
	logicPortUIName[195] = "Out187";
	
	logicPortType[196] = 0;
	logicPortPos[196] = "0 0 121";
	logicPortDir[196] = 1;
	logicPortUIName[196] = "Out188";
	
	logicPortType[197] = 0;
	logicPortPos[197] = "0 0 123";
	logicPortDir[197] = 1;
	logicPortUIName[197] = "Out189";
	
	logicPortType[198] = 0;
	logicPortPos[198] = "0 0 125";
	logicPortDir[198] = 1;
	logicPortUIName[198] = "Out190";
	
	logicPortType[199] = 0;
	logicPortPos[199] = "0 0 127";
	logicPortDir[199] = 1;
	logicPortUIName[199] = "Out191";
	
	logicPortType[200] = 0;
	logicPortPos[200] = "0 0 129";
	logicPortDir[200] = 1;
	logicPortUIName[200] = "Out192";
	
	logicPortType[201] = 0;
	logicPortPos[201] = "0 0 131";
	logicPortDir[201] = 1;
	logicPortUIName[201] = "Out193";
	
	logicPortType[202] = 0;
	logicPortPos[202] = "0 0 133";
	logicPortDir[202] = 1;
	logicPortUIName[202] = "Out194";
	
	logicPortType[203] = 0;
	logicPortPos[203] = "0 0 135";
	logicPortDir[203] = 1;
	logicPortUIName[203] = "Out195";
	
	logicPortType[204] = 0;
	logicPortPos[204] = "0 0 137";
	logicPortDir[204] = 1;
	logicPortUIName[204] = "Out196";
	
	logicPortType[205] = 0;
	logicPortPos[205] = "0 0 139";
	logicPortDir[205] = 1;
	logicPortUIName[205] = "Out197";
	
	logicPortType[206] = 0;
	logicPortPos[206] = "0 0 141";
	logicPortDir[206] = 1;
	logicPortUIName[206] = "Out198";
	
	logicPortType[207] = 0;
	logicPortPos[207] = "0 0 143";
	logicPortDir[207] = 1;
	logicPortUIName[207] = "Out199";
	
	logicPortType[208] = 0;
	logicPortPos[208] = "0 0 145";
	logicPortDir[208] = 1;
	logicPortUIName[208] = "Out200";
	
	logicPortType[209] = 0;
	logicPortPos[209] = "0 0 147";
	logicPortDir[209] = 1;
	logicPortUIName[209] = "Out201";
	
	logicPortType[210] = 0;
	logicPortPos[210] = "0 0 149";
	logicPortDir[210] = 1;
	logicPortUIName[210] = "Out202";
	
	logicPortType[211] = 0;
	logicPortPos[211] = "0 0 151";
	logicPortDir[211] = 1;
	logicPortUIName[211] = "Out203";
	
	logicPortType[212] = 0;
	logicPortPos[212] = "0 0 153";
	logicPortDir[212] = 1;
	logicPortUIName[212] = "Out204";
	
	logicPortType[213] = 0;
	logicPortPos[213] = "0 0 155";
	logicPortDir[213] = 1;
	logicPortUIName[213] = "Out205";
	
	logicPortType[214] = 0;
	logicPortPos[214] = "0 0 157";
	logicPortDir[214] = 1;
	logicPortUIName[214] = "Out206";
	
	logicPortType[215] = 0;
	logicPortPos[215] = "0 0 159";
	logicPortDir[215] = 1;
	logicPortUIName[215] = "Out207";
	
	logicPortType[216] = 0;
	logicPortPos[216] = "0 0 161";
	logicPortDir[216] = 1;
	logicPortUIName[216] = "Out208";
	
	logicPortType[217] = 0;
	logicPortPos[217] = "0 0 163";
	logicPortDir[217] = 1;
	logicPortUIName[217] = "Out209";
	
	logicPortType[218] = 0;
	logicPortPos[218] = "0 0 165";
	logicPortDir[218] = 1;
	logicPortUIName[218] = "Out210";
	
	logicPortType[219] = 0;
	logicPortPos[219] = "0 0 167";
	logicPortDir[219] = 1;
	logicPortUIName[219] = "Out211";
	
	logicPortType[220] = 0;
	logicPortPos[220] = "0 0 169";
	logicPortDir[220] = 1;
	logicPortUIName[220] = "Out212";
	
	logicPortType[221] = 0;
	logicPortPos[221] = "0 0 171";
	logicPortDir[221] = 1;
	logicPortUIName[221] = "Out213";
	
	logicPortType[222] = 0;
	logicPortPos[222] = "0 0 173";
	logicPortDir[222] = 1;
	logicPortUIName[222] = "Out214";
	
	logicPortType[223] = 0;
	logicPortPos[223] = "0 0 175";
	logicPortDir[223] = 1;
	logicPortUIName[223] = "Out215";
	
	logicPortType[224] = 0;
	logicPortPos[224] = "0 0 177";
	logicPortDir[224] = 1;
	logicPortUIName[224] = "Out216";
	
	logicPortType[225] = 0;
	logicPortPos[225] = "0 0 179";
	logicPortDir[225] = 1;
	logicPortUIName[225] = "Out217";
	
	logicPortType[226] = 0;
	logicPortPos[226] = "0 0 181";
	logicPortDir[226] = 1;
	logicPortUIName[226] = "Out218";
	
	logicPortType[227] = 0;
	logicPortPos[227] = "0 0 183";
	logicPortDir[227] = 1;
	logicPortUIName[227] = "Out219";
	
	logicPortType[228] = 0;
	logicPortPos[228] = "0 0 185";
	logicPortDir[228] = 1;
	logicPortUIName[228] = "Out220";
	
	logicPortType[229] = 0;
	logicPortPos[229] = "0 0 187";
	logicPortDir[229] = 1;
	logicPortUIName[229] = "Out221";
	
	logicPortType[230] = 0;
	logicPortPos[230] = "0 0 189";
	logicPortDir[230] = 1;
	logicPortUIName[230] = "Out222";
	
	logicPortType[231] = 0;
	logicPortPos[231] = "0 0 191";
	logicPortDir[231] = 1;
	logicPortUIName[231] = "Out223";
	
	logicPortType[232] = 0;
	logicPortPos[232] = "0 0 193";
	logicPortDir[232] = 1;
	logicPortUIName[232] = "Out224";
	
	logicPortType[233] = 0;
	logicPortPos[233] = "0 0 195";
	logicPortDir[233] = 1;
	logicPortUIName[233] = "Out225";
	
	logicPortType[234] = 0;
	logicPortPos[234] = "0 0 197";
	logicPortDir[234] = 1;
	logicPortUIName[234] = "Out226";
	
	logicPortType[235] = 0;
	logicPortPos[235] = "0 0 199";
	logicPortDir[235] = 1;
	logicPortUIName[235] = "Out227";
	
	logicPortType[236] = 0;
	logicPortPos[236] = "0 0 201";
	logicPortDir[236] = 1;
	logicPortUIName[236] = "Out228";
	
	logicPortType[237] = 0;
	logicPortPos[237] = "0 0 203";
	logicPortDir[237] = 1;
	logicPortUIName[237] = "Out229";
	
	logicPortType[238] = 0;
	logicPortPos[238] = "0 0 205";
	logicPortDir[238] = 1;
	logicPortUIName[238] = "Out230";
	
	logicPortType[239] = 0;
	logicPortPos[239] = "0 0 207";
	logicPortDir[239] = 1;
	logicPortUIName[239] = "Out231";
	
	logicPortType[240] = 0;
	logicPortPos[240] = "0 0 209";
	logicPortDir[240] = 1;
	logicPortUIName[240] = "Out232";
	
	logicPortType[241] = 0;
	logicPortPos[241] = "0 0 211";
	logicPortDir[241] = 1;
	logicPortUIName[241] = "Out233";
	
	logicPortType[242] = 0;
	logicPortPos[242] = "0 0 213";
	logicPortDir[242] = 1;
	logicPortUIName[242] = "Out234";
	
	logicPortType[243] = 0;
	logicPortPos[243] = "0 0 215";
	logicPortDir[243] = 1;
	logicPortUIName[243] = "Out235";
	
	logicPortType[244] = 0;
	logicPortPos[244] = "0 0 217";
	logicPortDir[244] = 1;
	logicPortUIName[244] = "Out236";
	
	logicPortType[245] = 0;
	logicPortPos[245] = "0 0 219";
	logicPortDir[245] = 1;
	logicPortUIName[245] = "Out237";
	
	logicPortType[246] = 0;
	logicPortPos[246] = "0 0 221";
	logicPortDir[246] = 1;
	logicPortUIName[246] = "Out238";
	
	logicPortType[247] = 0;
	logicPortPos[247] = "0 0 223";
	logicPortDir[247] = 1;
	logicPortUIName[247] = "Out239";
	
	logicPortType[248] = 0;
	logicPortPos[248] = "0 0 225";
	logicPortDir[248] = 1;
	logicPortUIName[248] = "Out240";
	
	logicPortType[249] = 0;
	logicPortPos[249] = "0 0 227";
	logicPortDir[249] = 1;
	logicPortUIName[249] = "Out241";
	
	logicPortType[250] = 0;
	logicPortPos[250] = "0 0 229";
	logicPortDir[250] = 1;
	logicPortUIName[250] = "Out242";
	
	logicPortType[251] = 0;
	logicPortPos[251] = "0 0 231";
	logicPortDir[251] = 1;
	logicPortUIName[251] = "Out243";
	
	logicPortType[252] = 0;
	logicPortPos[252] = "0 0 233";
	logicPortDir[252] = 1;
	logicPortUIName[252] = "Out244";
	
	logicPortType[253] = 0;
	logicPortPos[253] = "0 0 235";
	logicPortDir[253] = 1;
	logicPortUIName[253] = "Out245";
	
	logicPortType[254] = 0;
	logicPortPos[254] = "0 0 237";
	logicPortDir[254] = 1;
	logicPortUIName[254] = "Out246";
	
	logicPortType[255] = 0;
	logicPortPos[255] = "0 0 239";
	logicPortDir[255] = 1;
	logicPortUIName[255] = "Out247";
	
	logicPortType[256] = 0;
	logicPortPos[256] = "0 0 241";
	logicPortDir[256] = 1;
	logicPortUIName[256] = "Out248";
	
	logicPortType[257] = 0;
	logicPortPos[257] = "0 0 243";
	logicPortDir[257] = 1;
	logicPortUIName[257] = "Out249";
	
	logicPortType[258] = 0;
	logicPortPos[258] = "0 0 245";
	logicPortDir[258] = 1;
	logicPortUIName[258] = "Out250";
	
	logicPortType[259] = 0;
	logicPortPos[259] = "0 0 247";
	logicPortDir[259] = 1;
	logicPortUIName[259] = "Out251";
	
	logicPortType[260] = 0;
	logicPortPos[260] = "0 0 249";
	logicPortDir[260] = 1;
	logicPortUIName[260] = "Out252";
	
	logicPortType[261] = 0;
	logicPortPos[261] = "0 0 251";
	logicPortDir[261] = 1;
	logicPortUIName[261] = "Out253";
	
	logicPortType[262] = 0;
	logicPortPos[262] = "0 0 253";
	logicPortDir[262] = 1;
	logicPortUIName[262] = "Out254";
	
	logicPortType[263] = 0;
	logicPortPos[263] = "0 0 255";
	logicPortDir[263] = 1;
	logicPortUIName[263] = "Out255";
	
	logicPortType[264] = 1;
	logicPortPos[264] = "0 0 -255";
	logicPortDir[264] = 5;
	logicPortUIName[264] = "Enable";
	logicPortCauseUpdate[264] = true;
	
};
