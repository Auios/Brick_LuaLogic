
datablock fxDtsBrickData(LogicGate_Buffer2Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 2 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 2 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 2 Bit";
	logicUIName = "Buffer 2 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 5)~=0 then " @
		"		Gate.setportstate(gate, 3, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 4, Gate.getportstate(gate, 2)) " @
		"	else " @
		"		Gate.setportstate(gate, 3, 0) " @
		"		Gate.setportstate(gate, 4, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 5;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "-1 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 0;
	logicPortPos[2] = "1 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "Out0";
	
	logicPortType[3] = 0;
	logicPortPos[3] = "-1 0 0";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "Out1";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "1 0 0";
	logicPortDir[4] = 2;
	logicPortUIName[4] = "Clock";
	logicPortCauseUpdate[4] = true;
	
};
