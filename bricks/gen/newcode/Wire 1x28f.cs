
datablock fxDtsBrickData(LogicWire_1x28f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x28f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x28f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x28f";
	
	logicBrickSize = "1 28 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
