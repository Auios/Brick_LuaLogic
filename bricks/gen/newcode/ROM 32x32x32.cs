
datablock fxDtsBrickData(LogicGate_Rom32x32x32_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 32x32x32.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 32x32x32";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 32x32x32";
	logicUIName = "ROM 32x32x32";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 32 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 32767 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 43;
	
	isLogicRom = true;
	logicRomY = 32;
	logicRomZ = 32;
	logicRomX = 32;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 -31 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 -31 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 -31 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 -31 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 -31 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "21 -31 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "19 -31 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "17 -31 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "15 -31 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "13 -31 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "31 31 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O0";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "29 31 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O1";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "27 31 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O2";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "25 31 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O3";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "23 31 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O4";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "21 31 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O5";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "19 31 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O6";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "17 31 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O7";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "15 31 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O8";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "13 31 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O9";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "11 31 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O10";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "9 31 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O11";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "7 31 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O12";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "5 31 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O13";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "3 31 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O14";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "1 31 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O15";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "-1 31 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O16";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "-3 31 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O17";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "-5 31 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O18";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "-7 31 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O19";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "-9 31 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O20";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "-11 31 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O21";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "-13 31 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O22";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "-15 31 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O23";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "-17 31 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O24";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "-19 31 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O25";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "-21 31 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O26";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "-23 31 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O27";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-25 31 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O28";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "-27 31 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O29";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "-29 31 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O30";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "-31 31 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "O31";
	
	logicPortType[42] = 1;
	logicPortPos[42] = "31 -31 0";
	logicPortDir[42] = 2;
	logicPortUIName[42] = "Clock";
	logicPortCauseUpdate[42] = true;
	
};
