
datablock fxDtsBrickData(LogicGate_Mux7Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 7 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 7 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 7 Bit Vertical";
	logicUIName = "Mux 7 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 128";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 136) then " @
		"		local idx = 8 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) + " @
		"			(Gate.getportstate(gate, 6) * 32) + " @
		"			(Gate.getportstate(gate, 7) * 64) " @
		"		Gate.setportstate(gate, 137, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 137, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 137;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -127";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -125";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -123";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -121";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -119";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -117";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "Sel5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 0 -115";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "Sel6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 0 -127";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "In0";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "0 0 -125";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "In1";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "0 0 -123";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "In2";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "0 0 -121";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "In3";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "0 0 -119";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "In4";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "0 0 -117";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "In5";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "0 0 -115";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "In6";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "0 0 -113";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "In7";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "0 0 -111";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "In8";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "0 0 -109";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "In9";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "0 0 -107";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "In10";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "0 0 -105";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "In11";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "0 0 -103";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "In12";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "0 0 -101";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "In13";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "0 0 -99";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "In14";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "0 0 -97";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "In15";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "0 0 -95";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "In16";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "0 0 -93";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "In17";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "0 0 -91";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "In18";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "0 0 -89";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "In19";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "0 0 -87";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "In20";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "0 0 -85";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "In21";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "0 0 -83";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "In22";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "0 0 -81";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "In23";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "0 0 -79";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "In24";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "0 0 -77";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "In25";
	
	logicPortType[33] = 1;
	logicPortPos[33] = "0 0 -75";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "In26";
	
	logicPortType[34] = 1;
	logicPortPos[34] = "0 0 -73";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "In27";
	
	logicPortType[35] = 1;
	logicPortPos[35] = "0 0 -71";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "In28";
	
	logicPortType[36] = 1;
	logicPortPos[36] = "0 0 -69";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "In29";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "0 0 -67";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "In30";
	
	logicPortType[38] = 1;
	logicPortPos[38] = "0 0 -65";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "In31";
	
	logicPortType[39] = 1;
	logicPortPos[39] = "0 0 -63";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "In32";
	
	logicPortType[40] = 1;
	logicPortPos[40] = "0 0 -61";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "In33";
	
	logicPortType[41] = 1;
	logicPortPos[41] = "0 0 -59";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "In34";
	
	logicPortType[42] = 1;
	logicPortPos[42] = "0 0 -57";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "In35";
	
	logicPortType[43] = 1;
	logicPortPos[43] = "0 0 -55";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "In36";
	
	logicPortType[44] = 1;
	logicPortPos[44] = "0 0 -53";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "In37";
	
	logicPortType[45] = 1;
	logicPortPos[45] = "0 0 -51";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "In38";
	
	logicPortType[46] = 1;
	logicPortPos[46] = "0 0 -49";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "In39";
	
	logicPortType[47] = 1;
	logicPortPos[47] = "0 0 -47";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "In40";
	
	logicPortType[48] = 1;
	logicPortPos[48] = "0 0 -45";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "In41";
	
	logicPortType[49] = 1;
	logicPortPos[49] = "0 0 -43";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "In42";
	
	logicPortType[50] = 1;
	logicPortPos[50] = "0 0 -41";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "In43";
	
	logicPortType[51] = 1;
	logicPortPos[51] = "0 0 -39";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "In44";
	
	logicPortType[52] = 1;
	logicPortPos[52] = "0 0 -37";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "In45";
	
	logicPortType[53] = 1;
	logicPortPos[53] = "0 0 -35";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "In46";
	
	logicPortType[54] = 1;
	logicPortPos[54] = "0 0 -33";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "In47";
	
	logicPortType[55] = 1;
	logicPortPos[55] = "0 0 -31";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "In48";
	
	logicPortType[56] = 1;
	logicPortPos[56] = "0 0 -29";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "In49";
	
	logicPortType[57] = 1;
	logicPortPos[57] = "0 0 -27";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "In50";
	
	logicPortType[58] = 1;
	logicPortPos[58] = "0 0 -25";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "In51";
	
	logicPortType[59] = 1;
	logicPortPos[59] = "0 0 -23";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "In52";
	
	logicPortType[60] = 1;
	logicPortPos[60] = "0 0 -21";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "In53";
	
	logicPortType[61] = 1;
	logicPortPos[61] = "0 0 -19";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "In54";
	
	logicPortType[62] = 1;
	logicPortPos[62] = "0 0 -17";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "In55";
	
	logicPortType[63] = 1;
	logicPortPos[63] = "0 0 -15";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "In56";
	
	logicPortType[64] = 1;
	logicPortPos[64] = "0 0 -13";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "In57";
	
	logicPortType[65] = 1;
	logicPortPos[65] = "0 0 -11";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "In58";
	
	logicPortType[66] = 1;
	logicPortPos[66] = "0 0 -9";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "In59";
	
	logicPortType[67] = 1;
	logicPortPos[67] = "0 0 -7";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "In60";
	
	logicPortType[68] = 1;
	logicPortPos[68] = "0 0 -5";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "In61";
	
	logicPortType[69] = 1;
	logicPortPos[69] = "0 0 -3";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "In62";
	
	logicPortType[70] = 1;
	logicPortPos[70] = "0 0 -1";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "In63";
	
	logicPortType[71] = 1;
	logicPortPos[71] = "0 0 1";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "In64";
	
	logicPortType[72] = 1;
	logicPortPos[72] = "0 0 3";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "In65";
	
	logicPortType[73] = 1;
	logicPortPos[73] = "0 0 5";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "In66";
	
	logicPortType[74] = 1;
	logicPortPos[74] = "0 0 7";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "In67";
	
	logicPortType[75] = 1;
	logicPortPos[75] = "0 0 9";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "In68";
	
	logicPortType[76] = 1;
	logicPortPos[76] = "0 0 11";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "In69";
	
	logicPortType[77] = 1;
	logicPortPos[77] = "0 0 13";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "In70";
	
	logicPortType[78] = 1;
	logicPortPos[78] = "0 0 15";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "In71";
	
	logicPortType[79] = 1;
	logicPortPos[79] = "0 0 17";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "In72";
	
	logicPortType[80] = 1;
	logicPortPos[80] = "0 0 19";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "In73";
	
	logicPortType[81] = 1;
	logicPortPos[81] = "0 0 21";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "In74";
	
	logicPortType[82] = 1;
	logicPortPos[82] = "0 0 23";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "In75";
	
	logicPortType[83] = 1;
	logicPortPos[83] = "0 0 25";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "In76";
	
	logicPortType[84] = 1;
	logicPortPos[84] = "0 0 27";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "In77";
	
	logicPortType[85] = 1;
	logicPortPos[85] = "0 0 29";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "In78";
	
	logicPortType[86] = 1;
	logicPortPos[86] = "0 0 31";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "In79";
	
	logicPortType[87] = 1;
	logicPortPos[87] = "0 0 33";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "In80";
	
	logicPortType[88] = 1;
	logicPortPos[88] = "0 0 35";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "In81";
	
	logicPortType[89] = 1;
	logicPortPos[89] = "0 0 37";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "In82";
	
	logicPortType[90] = 1;
	logicPortPos[90] = "0 0 39";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "In83";
	
	logicPortType[91] = 1;
	logicPortPos[91] = "0 0 41";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "In84";
	
	logicPortType[92] = 1;
	logicPortPos[92] = "0 0 43";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "In85";
	
	logicPortType[93] = 1;
	logicPortPos[93] = "0 0 45";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "In86";
	
	logicPortType[94] = 1;
	logicPortPos[94] = "0 0 47";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "In87";
	
	logicPortType[95] = 1;
	logicPortPos[95] = "0 0 49";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "In88";
	
	logicPortType[96] = 1;
	logicPortPos[96] = "0 0 51";
	logicPortDir[96] = 1;
	logicPortUIName[96] = "In89";
	
	logicPortType[97] = 1;
	logicPortPos[97] = "0 0 53";
	logicPortDir[97] = 1;
	logicPortUIName[97] = "In90";
	
	logicPortType[98] = 1;
	logicPortPos[98] = "0 0 55";
	logicPortDir[98] = 1;
	logicPortUIName[98] = "In91";
	
	logicPortType[99] = 1;
	logicPortPos[99] = "0 0 57";
	logicPortDir[99] = 1;
	logicPortUIName[99] = "In92";
	
	logicPortType[100] = 1;
	logicPortPos[100] = "0 0 59";
	logicPortDir[100] = 1;
	logicPortUIName[100] = "In93";
	
	logicPortType[101] = 1;
	logicPortPos[101] = "0 0 61";
	logicPortDir[101] = 1;
	logicPortUIName[101] = "In94";
	
	logicPortType[102] = 1;
	logicPortPos[102] = "0 0 63";
	logicPortDir[102] = 1;
	logicPortUIName[102] = "In95";
	
	logicPortType[103] = 1;
	logicPortPos[103] = "0 0 65";
	logicPortDir[103] = 1;
	logicPortUIName[103] = "In96";
	
	logicPortType[104] = 1;
	logicPortPos[104] = "0 0 67";
	logicPortDir[104] = 1;
	logicPortUIName[104] = "In97";
	
	logicPortType[105] = 1;
	logicPortPos[105] = "0 0 69";
	logicPortDir[105] = 1;
	logicPortUIName[105] = "In98";
	
	logicPortType[106] = 1;
	logicPortPos[106] = "0 0 71";
	logicPortDir[106] = 1;
	logicPortUIName[106] = "In99";
	
	logicPortType[107] = 1;
	logicPortPos[107] = "0 0 73";
	logicPortDir[107] = 1;
	logicPortUIName[107] = "In100";
	
	logicPortType[108] = 1;
	logicPortPos[108] = "0 0 75";
	logicPortDir[108] = 1;
	logicPortUIName[108] = "In101";
	
	logicPortType[109] = 1;
	logicPortPos[109] = "0 0 77";
	logicPortDir[109] = 1;
	logicPortUIName[109] = "In102";
	
	logicPortType[110] = 1;
	logicPortPos[110] = "0 0 79";
	logicPortDir[110] = 1;
	logicPortUIName[110] = "In103";
	
	logicPortType[111] = 1;
	logicPortPos[111] = "0 0 81";
	logicPortDir[111] = 1;
	logicPortUIName[111] = "In104";
	
	logicPortType[112] = 1;
	logicPortPos[112] = "0 0 83";
	logicPortDir[112] = 1;
	logicPortUIName[112] = "In105";
	
	logicPortType[113] = 1;
	logicPortPos[113] = "0 0 85";
	logicPortDir[113] = 1;
	logicPortUIName[113] = "In106";
	
	logicPortType[114] = 1;
	logicPortPos[114] = "0 0 87";
	logicPortDir[114] = 1;
	logicPortUIName[114] = "In107";
	
	logicPortType[115] = 1;
	logicPortPos[115] = "0 0 89";
	logicPortDir[115] = 1;
	logicPortUIName[115] = "In108";
	
	logicPortType[116] = 1;
	logicPortPos[116] = "0 0 91";
	logicPortDir[116] = 1;
	logicPortUIName[116] = "In109";
	
	logicPortType[117] = 1;
	logicPortPos[117] = "0 0 93";
	logicPortDir[117] = 1;
	logicPortUIName[117] = "In110";
	
	logicPortType[118] = 1;
	logicPortPos[118] = "0 0 95";
	logicPortDir[118] = 1;
	logicPortUIName[118] = "In111";
	
	logicPortType[119] = 1;
	logicPortPos[119] = "0 0 97";
	logicPortDir[119] = 1;
	logicPortUIName[119] = "In112";
	
	logicPortType[120] = 1;
	logicPortPos[120] = "0 0 99";
	logicPortDir[120] = 1;
	logicPortUIName[120] = "In113";
	
	logicPortType[121] = 1;
	logicPortPos[121] = "0 0 101";
	logicPortDir[121] = 1;
	logicPortUIName[121] = "In114";
	
	logicPortType[122] = 1;
	logicPortPos[122] = "0 0 103";
	logicPortDir[122] = 1;
	logicPortUIName[122] = "In115";
	
	logicPortType[123] = 1;
	logicPortPos[123] = "0 0 105";
	logicPortDir[123] = 1;
	logicPortUIName[123] = "In116";
	
	logicPortType[124] = 1;
	logicPortPos[124] = "0 0 107";
	logicPortDir[124] = 1;
	logicPortUIName[124] = "In117";
	
	logicPortType[125] = 1;
	logicPortPos[125] = "0 0 109";
	logicPortDir[125] = 1;
	logicPortUIName[125] = "In118";
	
	logicPortType[126] = 1;
	logicPortPos[126] = "0 0 111";
	logicPortDir[126] = 1;
	logicPortUIName[126] = "In119";
	
	logicPortType[127] = 1;
	logicPortPos[127] = "0 0 113";
	logicPortDir[127] = 1;
	logicPortUIName[127] = "In120";
	
	logicPortType[128] = 1;
	logicPortPos[128] = "0 0 115";
	logicPortDir[128] = 1;
	logicPortUIName[128] = "In121";
	
	logicPortType[129] = 1;
	logicPortPos[129] = "0 0 117";
	logicPortDir[129] = 1;
	logicPortUIName[129] = "In122";
	
	logicPortType[130] = 1;
	logicPortPos[130] = "0 0 119";
	logicPortDir[130] = 1;
	logicPortUIName[130] = "In123";
	
	logicPortType[131] = 1;
	logicPortPos[131] = "0 0 121";
	logicPortDir[131] = 1;
	logicPortUIName[131] = "In124";
	
	logicPortType[132] = 1;
	logicPortPos[132] = "0 0 123";
	logicPortDir[132] = 1;
	logicPortUIName[132] = "In125";
	
	logicPortType[133] = 1;
	logicPortPos[133] = "0 0 125";
	logicPortDir[133] = 1;
	logicPortUIName[133] = "In126";
	
	logicPortType[134] = 1;
	logicPortPos[134] = "0 0 127";
	logicPortDir[134] = 1;
	logicPortUIName[134] = "In127";
	
	logicPortType[135] = 1;
	logicPortPos[135] = "0 0 -127";
	logicPortDir[135] = 5;
	logicPortUIName[135] = "Enable";
	logicPortCauseUpdate[135] = true;
	
	logicPortType[136] = 0;
	logicPortPos[136] = "0 0 127";
	logicPortDir[136] = 4;
	logicPortUIName[136] = "Out";
	
};
