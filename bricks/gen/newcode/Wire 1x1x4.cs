
datablock fxDtsBrickData(LogicWire_1x1x4_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x4.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x4";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x4";
	
	logicBrickSize = "1 1 12";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
