
datablock fxDtsBrickData(LogicGate_Rom32x8x32_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 32x8x32.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 32x8x32";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 32x8x32";
	logicUIName = "ROM 32x8x32";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 8 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 8191 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 41;
	
	isLogicRom = true;
	logicRomY = 8;
	logicRomZ = 32;
	logicRomX = 32;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 -7 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 -7 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 -7 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 -7 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 -7 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "21 -7 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "19 -7 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "17 -7 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "31 7 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O0";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "29 7 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O1";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "27 7 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O2";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "25 7 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O3";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "23 7 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O4";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "21 7 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O5";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "19 7 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O6";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "17 7 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O7";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "15 7 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O8";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "13 7 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O9";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "11 7 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O10";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "9 7 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O11";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "7 7 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O12";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "5 7 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O13";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "3 7 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O14";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "1 7 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O15";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "-1 7 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O16";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "-3 7 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O17";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "-5 7 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O18";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "-7 7 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O19";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "-9 7 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O20";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "-11 7 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O21";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "-13 7 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O22";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "-15 7 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O23";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "-17 7 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O24";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "-19 7 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O25";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "-21 7 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O26";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "-23 7 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O27";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "-25 7 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O28";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "-27 7 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O29";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-29 7 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O30";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "-31 7 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O31";
	
	logicPortType[40] = 1;
	logicPortPos[40] = "31 -7 0";
	logicPortDir[40] = 2;
	logicPortUIName[40] = "Clock";
	logicPortCauseUpdate[40] = true;
	
};
