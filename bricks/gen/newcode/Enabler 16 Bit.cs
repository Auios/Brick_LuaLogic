
datablock fxDtsBrickData(LogicGate_Enabler16Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Enabler 16 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Enabler 16 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Enabler 16 Bit";
	logicUIName = "Enabler 16 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "16 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 33)~=0 then " @
		"		Gate.setportstate(gate, 17, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 18, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 19, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 20, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 21, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 22, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 23, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 24, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 25, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 26, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 27, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 28, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 29, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 30, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 31, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 32, Gate.getportstate(gate, 16)) " @
		"	else " @
		"		Gate.setportstate(gate, 17, 0) " @
		"		Gate.setportstate(gate, 18, 0) " @
		"		Gate.setportstate(gate, 19, 0) " @
		"		Gate.setportstate(gate, 20, 0) " @
		"		Gate.setportstate(gate, 21, 0) " @
		"		Gate.setportstate(gate, 22, 0) " @
		"		Gate.setportstate(gate, 23, 0) " @
		"		Gate.setportstate(gate, 24, 0) " @
		"		Gate.setportstate(gate, 25, 0) " @
		"		Gate.setportstate(gate, 26, 0) " @
		"		Gate.setportstate(gate, 27, 0) " @
		"		Gate.setportstate(gate, 28, 0) " @
		"		Gate.setportstate(gate, 29, 0) " @
		"		Gate.setportstate(gate, 30, 0) " @
		"		Gate.setportstate(gate, 31, 0) " @
		"		Gate.setportstate(gate, 32, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 33;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "15 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "13 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "11 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "9 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "7 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "5 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "3 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "1 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-1 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "-3 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "-5 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "-7 0 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "In11";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "-9 0 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "In12";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "-11 0 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "In13";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "-13 0 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "In14";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "-15 0 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "In15";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 0;
	logicPortPos[16] = "15 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out0";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "13 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out1";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "11 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out2";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "9 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out3";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "7 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out4";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "5 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out5";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "3 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out6";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "1 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out7";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "-1 0 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out8";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "-3 0 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out9";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "-5 0 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out10";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "-7 0 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out11";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "-9 0 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out12";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "-11 0 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out13";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "-13 0 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "Out14";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "-15 0 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "Out15";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "15 0 0";
	logicPortDir[32] = 2;
	logicPortUIName[32] = "Clock";
	logicPortCauseUpdate[32] = true;
	
};
