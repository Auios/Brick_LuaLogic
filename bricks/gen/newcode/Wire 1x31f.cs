
datablock fxDtsBrickData(LogicWire_1x31f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x31f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x31f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x31f";
	
	logicBrickSize = "1 31 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
