
datablock fxDtsBrickData(LogicWire_1x46f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x46f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x46f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x46f";
	
	logicBrickSize = "1 46 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
