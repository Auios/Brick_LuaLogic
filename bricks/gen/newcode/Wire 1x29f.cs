
datablock fxDtsBrickData(LogicWire_1x29f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x29f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x29f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x29f";
	
	logicBrickSize = "1 29 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
