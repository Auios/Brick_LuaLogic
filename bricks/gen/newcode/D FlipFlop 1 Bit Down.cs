
datablock fxDtsBrickData(LogicGate_DFlipFlop1BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 1 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 1 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 1 Bit Down";
	logicUIName = "D FlipFlop 1 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 3)~=0 then " @
		"		Gate.setportstate(gate, 2, Gate.getportstate(gate, 1)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 3;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 0;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = 5;
	logicPortUIName[1] = "Out0";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 0";
	logicPortDir[2] = 2;
	logicPortUIName[2] = "Clock";
	logicPortCauseUpdate[2] = true;
	
};
