
datablock fxDtsBrickData(LogicWire_1x50f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x50f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x50f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x50f";
	
	logicBrickSize = "1 50 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
