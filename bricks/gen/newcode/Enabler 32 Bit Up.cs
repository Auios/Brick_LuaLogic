
datablock fxDtsBrickData(LogicGate_Enabler32BitUp_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Enabler 32 Bit Up.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Enabler 32 Bit Up";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Enabler 32 Bit Up";
	logicUIName = "Enabler 32 Bit Up";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 65)~=0 then " @
		"		Gate.setportstate(gate, 33, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 34, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 35, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 36, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 37, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 38, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 39, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 40, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 41, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 42, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 43, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 44, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 45, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 46, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 47, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 48, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 49, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 50, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 51, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 52, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 53, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 54, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 55, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 56, Gate.getportstate(gate, 24)) " @
		"		Gate.setportstate(gate, 57, Gate.getportstate(gate, 25)) " @
		"		Gate.setportstate(gate, 58, Gate.getportstate(gate, 26)) " @
		"		Gate.setportstate(gate, 59, Gate.getportstate(gate, 27)) " @
		"		Gate.setportstate(gate, 60, Gate.getportstate(gate, 28)) " @
		"		Gate.setportstate(gate, 61, Gate.getportstate(gate, 29)) " @
		"		Gate.setportstate(gate, 62, Gate.getportstate(gate, 30)) " @
		"		Gate.setportstate(gate, 63, Gate.getportstate(gate, 31)) " @
		"		Gate.setportstate(gate, 64, Gate.getportstate(gate, 32)) " @
		"	else " @
		"		Gate.setportstate(gate, 33, 0) " @
		"		Gate.setportstate(gate, 34, 0) " @
		"		Gate.setportstate(gate, 35, 0) " @
		"		Gate.setportstate(gate, 36, 0) " @
		"		Gate.setportstate(gate, 37, 0) " @
		"		Gate.setportstate(gate, 38, 0) " @
		"		Gate.setportstate(gate, 39, 0) " @
		"		Gate.setportstate(gate, 40, 0) " @
		"		Gate.setportstate(gate, 41, 0) " @
		"		Gate.setportstate(gate, 42, 0) " @
		"		Gate.setportstate(gate, 43, 0) " @
		"		Gate.setportstate(gate, 44, 0) " @
		"		Gate.setportstate(gate, 45, 0) " @
		"		Gate.setportstate(gate, 46, 0) " @
		"		Gate.setportstate(gate, 47, 0) " @
		"		Gate.setportstate(gate, 48, 0) " @
		"		Gate.setportstate(gate, 49, 0) " @
		"		Gate.setportstate(gate, 50, 0) " @
		"		Gate.setportstate(gate, 51, 0) " @
		"		Gate.setportstate(gate, 52, 0) " @
		"		Gate.setportstate(gate, 53, 0) " @
		"		Gate.setportstate(gate, 54, 0) " @
		"		Gate.setportstate(gate, 55, 0) " @
		"		Gate.setportstate(gate, 56, 0) " @
		"		Gate.setportstate(gate, 57, 0) " @
		"		Gate.setportstate(gate, 58, 0) " @
		"		Gate.setportstate(gate, 59, 0) " @
		"		Gate.setportstate(gate, 60, 0) " @
		"		Gate.setportstate(gate, 61, 0) " @
		"		Gate.setportstate(gate, 62, 0) " @
		"		Gate.setportstate(gate, 63, 0) " @
		"		Gate.setportstate(gate, 64, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 65;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 0 0";
	logicPortDir[0] = 5;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 0 0";
	logicPortDir[1] = 5;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 0 0";
	logicPortDir[2] = 5;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 0 0";
	logicPortDir[3] = 5;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 0 0";
	logicPortDir[4] = 5;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "21 0 0";
	logicPortDir[5] = 5;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "19 0 0";
	logicPortDir[6] = 5;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "17 0 0";
	logicPortDir[7] = 5;
	logicPortUIName[7] = "In7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "15 0 0";
	logicPortDir[8] = 5;
	logicPortUIName[8] = "In8";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "13 0 0";
	logicPortDir[9] = 5;
	logicPortUIName[9] = "In9";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "11 0 0";
	logicPortDir[10] = 5;
	logicPortUIName[10] = "In10";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "9 0 0";
	logicPortDir[11] = 5;
	logicPortUIName[11] = "In11";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "7 0 0";
	logicPortDir[12] = 5;
	logicPortUIName[12] = "In12";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "5 0 0";
	logicPortDir[13] = 5;
	logicPortUIName[13] = "In13";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "3 0 0";
	logicPortDir[14] = 5;
	logicPortUIName[14] = "In14";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "1 0 0";
	logicPortDir[15] = 5;
	logicPortUIName[15] = "In15";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 1;
	logicPortPos[16] = "-1 0 0";
	logicPortDir[16] = 5;
	logicPortUIName[16] = "In16";
	logicPortCauseUpdate[16] = true;
	
	logicPortType[17] = 1;
	logicPortPos[17] = "-3 0 0";
	logicPortDir[17] = 5;
	logicPortUIName[17] = "In17";
	logicPortCauseUpdate[17] = true;
	
	logicPortType[18] = 1;
	logicPortPos[18] = "-5 0 0";
	logicPortDir[18] = 5;
	logicPortUIName[18] = "In18";
	logicPortCauseUpdate[18] = true;
	
	logicPortType[19] = 1;
	logicPortPos[19] = "-7 0 0";
	logicPortDir[19] = 5;
	logicPortUIName[19] = "In19";
	logicPortCauseUpdate[19] = true;
	
	logicPortType[20] = 1;
	logicPortPos[20] = "-9 0 0";
	logicPortDir[20] = 5;
	logicPortUIName[20] = "In20";
	logicPortCauseUpdate[20] = true;
	
	logicPortType[21] = 1;
	logicPortPos[21] = "-11 0 0";
	logicPortDir[21] = 5;
	logicPortUIName[21] = "In21";
	logicPortCauseUpdate[21] = true;
	
	logicPortType[22] = 1;
	logicPortPos[22] = "-13 0 0";
	logicPortDir[22] = 5;
	logicPortUIName[22] = "In22";
	logicPortCauseUpdate[22] = true;
	
	logicPortType[23] = 1;
	logicPortPos[23] = "-15 0 0";
	logicPortDir[23] = 5;
	logicPortUIName[23] = "In23";
	logicPortCauseUpdate[23] = true;
	
	logicPortType[24] = 1;
	logicPortPos[24] = "-17 0 0";
	logicPortDir[24] = 5;
	logicPortUIName[24] = "In24";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 1;
	logicPortPos[25] = "-19 0 0";
	logicPortDir[25] = 5;
	logicPortUIName[25] = "In25";
	logicPortCauseUpdate[25] = true;
	
	logicPortType[26] = 1;
	logicPortPos[26] = "-21 0 0";
	logicPortDir[26] = 5;
	logicPortUIName[26] = "In26";
	logicPortCauseUpdate[26] = true;
	
	logicPortType[27] = 1;
	logicPortPos[27] = "-23 0 0";
	logicPortDir[27] = 5;
	logicPortUIName[27] = "In27";
	logicPortCauseUpdate[27] = true;
	
	logicPortType[28] = 1;
	logicPortPos[28] = "-25 0 0";
	logicPortDir[28] = 5;
	logicPortUIName[28] = "In28";
	logicPortCauseUpdate[28] = true;
	
	logicPortType[29] = 1;
	logicPortPos[29] = "-27 0 0";
	logicPortDir[29] = 5;
	logicPortUIName[29] = "In29";
	logicPortCauseUpdate[29] = true;
	
	logicPortType[30] = 1;
	logicPortPos[30] = "-29 0 0";
	logicPortDir[30] = 5;
	logicPortUIName[30] = "In30";
	logicPortCauseUpdate[30] = true;
	
	logicPortType[31] = 1;
	logicPortPos[31] = "-31 0 0";
	logicPortDir[31] = 5;
	logicPortUIName[31] = "In31";
	logicPortCauseUpdate[31] = true;
	
	logicPortType[32] = 0;
	logicPortPos[32] = "31 0 0";
	logicPortDir[32] = 4;
	logicPortUIName[32] = "Out0";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "29 0 0";
	logicPortDir[33] = 4;
	logicPortUIName[33] = "Out1";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "27 0 0";
	logicPortDir[34] = 4;
	logicPortUIName[34] = "Out2";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "25 0 0";
	logicPortDir[35] = 4;
	logicPortUIName[35] = "Out3";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "23 0 0";
	logicPortDir[36] = 4;
	logicPortUIName[36] = "Out4";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "21 0 0";
	logicPortDir[37] = 4;
	logicPortUIName[37] = "Out5";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "19 0 0";
	logicPortDir[38] = 4;
	logicPortUIName[38] = "Out6";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "17 0 0";
	logicPortDir[39] = 4;
	logicPortUIName[39] = "Out7";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "15 0 0";
	logicPortDir[40] = 4;
	logicPortUIName[40] = "Out8";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "13 0 0";
	logicPortDir[41] = 4;
	logicPortUIName[41] = "Out9";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "11 0 0";
	logicPortDir[42] = 4;
	logicPortUIName[42] = "Out10";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "9 0 0";
	logicPortDir[43] = 4;
	logicPortUIName[43] = "Out11";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "7 0 0";
	logicPortDir[44] = 4;
	logicPortUIName[44] = "Out12";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "5 0 0";
	logicPortDir[45] = 4;
	logicPortUIName[45] = "Out13";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "3 0 0";
	logicPortDir[46] = 4;
	logicPortUIName[46] = "Out14";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "1 0 0";
	logicPortDir[47] = 4;
	logicPortUIName[47] = "Out15";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-1 0 0";
	logicPortDir[48] = 4;
	logicPortUIName[48] = "Out16";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-3 0 0";
	logicPortDir[49] = 4;
	logicPortUIName[49] = "Out17";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-5 0 0";
	logicPortDir[50] = 4;
	logicPortUIName[50] = "Out18";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-7 0 0";
	logicPortDir[51] = 4;
	logicPortUIName[51] = "Out19";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-9 0 0";
	logicPortDir[52] = 4;
	logicPortUIName[52] = "Out20";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-11 0 0";
	logicPortDir[53] = 4;
	logicPortUIName[53] = "Out21";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-13 0 0";
	logicPortDir[54] = 4;
	logicPortUIName[54] = "Out22";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-15 0 0";
	logicPortDir[55] = 4;
	logicPortUIName[55] = "Out23";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-17 0 0";
	logicPortDir[56] = 4;
	logicPortUIName[56] = "Out24";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-19 0 0";
	logicPortDir[57] = 4;
	logicPortUIName[57] = "Out25";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-21 0 0";
	logicPortDir[58] = 4;
	logicPortUIName[58] = "Out26";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "-23 0 0";
	logicPortDir[59] = 4;
	logicPortUIName[59] = "Out27";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "-25 0 0";
	logicPortDir[60] = 4;
	logicPortUIName[60] = "Out28";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "-27 0 0";
	logicPortDir[61] = 4;
	logicPortUIName[61] = "Out29";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "-29 0 0";
	logicPortDir[62] = 4;
	logicPortUIName[62] = "Out30";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "-31 0 0";
	logicPortDir[63] = 4;
	logicPortUIName[63] = "Out31";
	
	logicPortType[64] = 1;
	logicPortPos[64] = "31 0 0";
	logicPortDir[64] = 2;
	logicPortUIName[64] = "Clock";
	logicPortCauseUpdate[64] = true;
	
};
