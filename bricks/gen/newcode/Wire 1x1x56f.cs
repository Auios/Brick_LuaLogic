
datablock fxDtsBrickData(LogicWire_1x1x56f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x56f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x56f";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x56f";
	
	logicBrickSize = "1 1 56";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
