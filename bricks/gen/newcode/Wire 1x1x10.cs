
datablock fxDtsBrickData(LogicWire_1x1x10_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x10.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x10";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x10";
	
	logicBrickSize = "1 1 30";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
