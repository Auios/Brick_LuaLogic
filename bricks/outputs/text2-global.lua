
textbrick2_bitsNeeded = 9

textbrick2_idxToPrint = {
	[0x00] = "space",
	[0x01] = "space",
	[0x02] = "space",
	[0x03] = "space",
	[0x04] = "space",
	[0x05] = "space",
	[0x06] = "space",
	[0x07] = "space",
	[0x08] = "space",
	[0x09] = "space",
	[0x0A] = "space",
	[0x0B] = "space",
	[0x0C] = "space",
	[0x0D] = "space",
	[0x0E] = "space",
	[0x0F] = "space",
	
	[0x10] = "color000",
	[0x11] = "color001",
	[0x12] = "color010",
	[0x13] = "color011",
	[0x14] = "color100",
	[0x15] = "color101",
	[0x16] = "color110",
	[0x17] = "color111",
	[0x18] = "space",
	[0x19] = "space",
	[0x1A] = "space",
	[0x1B] = "space",
	[0x1C] = "space",
	[0x1D] = "space",
	[0x1E] = "space",
	[0x1F] = "space",
	
	[0x20] = "space",
	[0x21] = "bang",
	[0x22] = "apostrophe2",
	[0x23] = "pound",
	[0x24] = "dollar",
	[0x25] = "percent",
	[0x26] = "and",
	[0x27] = "apostrophe",
	[0x28] = "roundbracketright",
	[0x29] = "roundbracketleft",
	[0x2A] = "asterisk",
	[0x2B] = "plus",
	[0x2C] = "comma",
	[0x2D] = "minus",
	[0x2E] = "period",
	[0x2F] = "slashright",
	
	[0x30] = "0",
	[0x31] = "1",
	[0x32] = "2",
	[0x33] = "3",
	[0x34] = "4",
	[0x35] = "5",
	[0x36] = "6",
	[0x37] = "7",
	[0x38] = "8",
	[0x39] = "9",
	[0x3A] = "colon",
	[0x3B] = "semicolon",
	[0x3C] = "less_than",
	[0x3D] = "equals",
	[0x3E] = "greater_than",
	[0x3F] = "qmark",
	
	[0x40] = "at",
	[0x41] = "A",
	[0x42] = "B",
	[0x43] = "C",
	[0x44] = "D",
	[0x45] = "E",
	[0x46] = "F",
	[0x47] = "G",
	[0x48] = "H",
	[0x49] = "I",
	[0x4A] = "J",
	[0x4B] = "K",
	[0x4C] = "L",
	[0x4D] = "M",
	[0x4E] = "N",
	[0x4F] = "O",
	
	[0x50] = "P",
	[0x51] = "Q",
	[0x52] = "R",
	[0x53] = "S",
	[0x54] = "T",
	[0x55] = "U",
	[0x56] = "V",
	[0x57] = "W",
	[0x58] = "X",
	[0x59] = "Y",
	[0x5A] = "Z",
	[0x5B] = "squarebracketleft",
	[0x5C] = "slashleft",
	[0x5D] = "squarebracketright",
	[0x5E] = "caret",
	[0x5F] = "underscore",
	
	[0x60] = "backtick",
	[0x61] = "Alcase",
	[0x62] = "Blcase",
	[0x63] = "Clcase",
	[0x64] = "Dlcase",
	[0x65] = "Elcase",
	[0x66] = "Flcase",
	[0x67] = "Glcase",
	[0x68] = "Hlcase",
	[0x69] = "Ilcase",
	[0x6A] = "Jlcase",
	[0x6B] = "Klcase",
	[0x6C] = "Llcase",
	[0x6D] = "Mlcase",
	[0x6E] = "Nlcase",
	[0x6F] = "Olcase",
	
	[0x70] = "Plcase",
	[0x71] = "Qlcase",
	[0x72] = "Rlcase",
	[0x73] = "Slcase",
	[0x74] = "Tlcase",
	[0x75] = "Ulcase",
	[0x76] = "Vlcase",
	[0x77] = "Wlcase",
	[0x78] = "Xlcase",
	[0x79] = "Ylcase",
	[0x7A] = "Zlcase",
	[0x7B] = "curlybracketleft",
	[0x7C] = "verticalbar",
	[0x7D] = "curlybracketright",
	[0x7E] = "tilde",
	[0x7F] = "space",
	
	[0x80] = "space",
	[0x81] = "space",
	[0x82] = "space",
	[0x83] = "space",
	[0x84] = "space",
	[0x85] = "space",
	[0x86] = "space",
	[0x87] = "space",
	[0x88] = "space",
	[0x89] = "space",
	[0x8A] = "space",
	[0x8B] = "space",
	[0x8C] = "space",
	[0x8D] = "space",
	[0x8E] = "space",
	[0x8F] = "space",
	
	[0x90] = "space",
	[0x91] = "space",
	[0x92] = "space",
	[0x93] = "space",
	[0x94] = "space",
	[0x95] = "space",
	[0x96] = "space",
	[0x97] = "space",
	[0x98] = "space",
	[0x99] = "space",
	[0x9A] = "space",
	[0x9B] = "space",
	[0x9C] = "space",
	[0x9D] = "space",
	[0x9E] = "space",
	[0x9F] = "space",
	
	[0xA0] = "jp-yen",
	[0xA1] = "jp-period",
	[0xA2] = "jp-left-bracket",
	[0xA3] = "jp-right-bracket",
	[0xA4] = "jp-comma",
	[0xA5] = "jp-dot",
	[0xA6] = "jp-wo",
	[0xA7] = "jp-small-a",
	[0xA8] = "jp-small-i",
	[0xA9] = "jp-small-u",
	[0xAA] = "jp-small-e",
	[0xAB] = "jp-small-o",
	[0xAC] = "jp-small-ya",
	[0xAD] = "jp-small-yu",
	[0xAE] = "jp-small-yo",
	[0xAF] = "jp-small-tsu",
	
	[0xB0] = "jp-dash",
	[0xB1] = "jp-a",
	[0xB2] = "jp-i",
	[0xB3] = "jp-u",
	[0xB4] = "jp-e",
	[0xB5] = "jp-o",
	[0xB6] = "jp-ka",
	[0xB7] = "jp-ki",
	[0xB8] = "jp-ku",
	[0xB9] = "jp-ke",
	[0xBA] = "jp-ko",
	[0xBB] = "jp-sa",
	[0xBC] = "jp-shi",
	[0xBD] = "jp-su",
	[0xBE] = "jp-se",
	[0xBF] = "jp-so",
	
	[0xC0] = "jp-ta",
	[0xC1] = "jp-chi",
	[0xC2] = "jp-tsu",
	[0xC3] = "jp-te",
	[0xC4] = "jp-to",
	[0xC5] = "jp-na",
	[0xC6] = "jp-ni",
	[0xC7] = "jp-nu",
	[0xC8] = "jp-ne",
	[0xC9] = "jp-no",
	[0xCA] = "jp-ha",
	[0xCB] = "jp-hi",
	[0xCC] = "jp-fu",
	[0xCD] = "jp-he",
	[0xCE] = "jp-ho",
	[0xCF] = "jp-ma",
	
	[0xD0] = "jp-mi",
	[0xD1] = "jp-mu",
	[0xD2] = "jp-me",
	[0xD3] = "jp-mo",
	[0xD4] = "jp-ya",
	[0xD5] = "jp-yu",
	[0xD6] = "jp-yo",
	[0xD7] = "jp-ra",
	[0xD8] = "jp-ri",
	[0xD9] = "jp-ru",
	[0xDA] = "jp-re",
	[0xDB] = "jp-ro",
	[0xDC] = "jp-wa",
	[0xDD] = "jp-n",
	[0xDE] = "jp-dakuten",
	[0xDF] = "jp-handakuten",
	
	[0xE0] = "space",
	[0xE1] = "space",
	[0xE2] = "space",
	[0xE3] = "space",
	[0xE4] = "space",
	[0xE5] = "space",
	[0xE6] = "space",
	[0xE7] = "space",
	[0xE8] = "space",
	[0xE9] = "space",
	[0xEA] = "space",
	[0xEB] = "space",
	[0xEC] = "space",
	[0xED] = "space",
	[0xEE] = "space",
	[0xEF] = "space",
	
	[0xF0] = "space",
	[0xF1] = "space",
	[0xF2] = "space",
	[0xF3] = "space",
	[0xF4] = "space",
	[0xF5] = "space",
	[0xF6] = "space",
	[0xF7] = "space",
	[0xF8] = "space",
	[0xF9] = "space",
	[0xFA] = "space",
	[0xFB] = "space",
	[0xFC] = "space",
	[0xFD] = "space",
	[0xFE] = "space",
	[0xFF] = "space",
}
