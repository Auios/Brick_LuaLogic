
if($Pref::Server::LuaLogic::OPT_TICK_ENABLED $= "") $Pref::Server::LuaLogic::OPT_TICK_ENABLED = true;
if($Pref::Server::LuaLogic::OPT_TICK_TIME    $= "") $Pref::Server::LuaLogic::OPT_TICK_TIME    = 0.032;
if($Pref::Server::LuaLogic::OPT_TICK_MULT    $= "") $Pref::Server::LuaLogic::OPT_TICK_MULT    = 1;
if($Pref::Server::LuaLogic::OPT_FX_UPDATES   $= "") $Pref::Server::LuaLogic::OPT_FX_UPDATES   = true;
if($Pref::Server::LuaLogic::OPT_FX_TIME      $= "") $Pref::Server::LuaLogic::OPT_FX_TIME      = 0.032;

exec("./utilities.cs");
exec("./tcp.cs");
exec("./bricks.cs");
exec("./brickdata.cs");
exec("./cmds.cs");

schedule(0, 0, lualogic_loadprintsandcolors);
schedule(0, 0, lualogic_registerAllGateDefinitions);

package LuaLogic
{
	function onServerDestroyed()
	{
		deleteVariables("$LuaLogic*");
		parent::onServerDestroyed();
	}
};
activatePackage("LuaLogic");

function llc(){
	lualogic_connect(25000);
}

function llr(){
	%path = $LuaLogic::Path;
	deleteVariables("$LuaLogic*");
	$LuaLogic::Path = %path;
	
	//resetAllOpCallFunc();
	exec("./lualogic.cs");
	//schedule(1000, 0, llc);
}

schedule(10000, 0, llc);
